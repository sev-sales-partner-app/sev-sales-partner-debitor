import { createElement as el } from '../util/view'
import { Title } from '../common'
import { PageLayout } from '../layout'

export function Home ({ state }) {
  return (
    el(PageLayout, {},
      el(Title, {}, 'Home Page Placeholder'),
      el('p', {}, 'This is the home placeholder'),
      el('pre', {}, `state: ${JSON.stringify(state, null, 2)}`)
    )
  )
}
