import {
  AccountPage,
  CreditorsPage,
  EntitlementsPage,
  // HomePage,
  SettlementsPage,
  SettlementPage,
  SignInPage, UserAccountPage
} from '../util/router'
import { createElement as el } from '../util/view'

import { Wrapper, Main, Footer, Header } from '../layout'
import { Home as Debug } from '../home'
import { Authentication } from '../authentication'
import { Creditors } from '../creditors'
import { Account } from '../account'
import { Settlement } from '../settlement'
import { NotFound } from '../notFound'

const pageMap = {
  'p-debug': Debug,
  [SignInPage]: Authentication,
  [CreditorsPage]: Creditors,
  [EntitlementsPage]: Creditors,
  [AccountPage]: Account,
  [UserAccountPage]: Account,
  [SettlementsPage]: Account,
  [SettlementPage]: Settlement
}

export function Root ({ actions, state }) {
  const { pageId } = state
  const Page = pageMap[pageId] || NotFound

  return (
    el(Wrapper, { id: pageId },
      el(Main, {},
        (pageId !== SignInPage) &&
          el(Header, { state }),

        el(Page, { actions, state })
      ),

      el(Footer, { actions, state })
    )
  )
}
