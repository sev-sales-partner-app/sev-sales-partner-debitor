import furyrpc from '../data/furyRpc'
import localstore from '../data/localStore'

export function fetchConsensus (state, fury = furyrpc, store = localstore) {
  return fury
    .fetchCurrentBlockNumber()
    .then(nblk => state.consensus.block < nblk
      ? store.matchBlockTime(nblk, fury)
      : state.consensus)
}

// const root = {}

export { Root } from './view'
