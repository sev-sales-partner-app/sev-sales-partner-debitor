import { assocPath, pipe, preventDefault } from '../util/fp'
import { b as css, createElement as el, theme } from '../util/view'
import { Input, Label, Hint } from '../common'
import { validateUsername } from '../validation/credentials'

const { space } = theme

export const credentials = {

  initialState: () => ({
    username: { value: '', error: '' }
  }),

  actions: update => ({
    handleCredentials: (id) => ev => {
      const form = ev.target
      const username = form.elements.username.value
      const callbackUrl = decodeURI(window.location.href) // form.elements.callbackUrl.value
      const validatedCreds = { username: validateUsername(username) }
      const errors =
        Object.values(validatedCreds).map(v => v.error).filter(Boolean)

      update(assocPath([id, 'username'])(validatedCreds.username))

      errors.length &&
        console.error('[handleFormData :: action]', 'validation errors:', errors)

      return { validatedCreds, callbackUrl, role: 'debtor', errors }
    },

    setUsername: (id, err) => ev => {
      const value = ev.target.value
      const username = err ? validateUsername(value) : { value, error: err }
      update(assocPath([ id, 'username' ])(username))
    }
  })
}

export function Credentials ({ actions, id = 'credentials', state }) {
  const wrapping = css`margin-top: ${space[3]}`
  // const callbackUrl = decodeURI(window.location.href)
  const { username } = state[id]

  return (
    el('form',
      { // noValidate: true,
        onSubmit:
          pipe(
            preventDefault,
            actions.handleCredentials(id),
            actions.submitSignInRequest) },

      el('div', { className: wrapping['class'] },
        el(Label, { htmlFor: 'username' }, 'User Name'),
        el(Input,
          { name: 'username',
            type: 'email',
            placeholder: 'beispiel@email.de',
            required: true,
            value: username.value,
            error: username.error,
            onChange: actions.setUsername(id, username.error) }),
        username.error && el(Hint, { error: username.error })
      ),

      el('div',
        { className: wrapping['class'] },
        el(Input, { type: 'submit', value: 'Sign in' })
      )
    )
  )
}
