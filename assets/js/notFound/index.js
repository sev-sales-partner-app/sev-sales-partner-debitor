// import { p } from '../util/debug'
import { HomePage, getUrl } from '../util/router'
import { createElement as el } from '../util/view'
import { Title } from '../common'
import { PageLayout } from '../layout'

export function NotFound () {
  return (
    el(PageLayout, {},

      el(Title, {}, 'Sorry, we could not find what you were looking 4...04'),

      el('a',
        { href: getUrl(HomePage) },
        'Return to Home Page')
    )
  )
}
