import { p } from '../util/debug'
import furyrpc from '../data/furyRpc'
import localstore from '../data/localStore'
import { assoc, assocPath, pipe } from '../util/fp'

export function actions (update, fury = furyrpc, store = localstore) {
  function fetchBalances (
    accounts = [],
    state,
    newConsensus,
    furyNode
  ) {
    p('[fetchAccountsBxs :: action :: creditors]',
      'set state._status to:',
      state._bxsLastFetch > 0 ? 'updating' : 'fetching')

    state._status === 'idle' &&
      update(assoc(['_status'])(
        state._bxsLastFetch > 0 ? 'updating' : 'fetching'
      ))

    return Promise
      .all(accounts.map(({ address, ledger, type }) =>
        fury.fetchAccountBalance(ledger, address, {
          n: furyNode,
          ledgerType:
            (type === 'settlement')
              ? 'transferable'
              : 'stromkonto'
        })
      ))
      .then(bxs => {
        const consensus = newConsensus || state.consensus

        p('[fetchAccountsBxs :: action :: creditors]',
          'patching balances of', accounts.length, 'accounts ')

        let patched = {}
        update(($tate) => {
          patched = pipe(
            assoc('_status')('idle'),
            assoc('_bxsLastFetch')(consensus.block),
            assocPath(['consensus', 'block'])(consensus.block),
            assocPath(['consensus', 'time'])(consensus.time),
            ...bxs.flatMap(bx => {
              const id = [bx.ledger, bx.address].join('::')
              return [
                assocPath(['accountsById', id, 'credit', 'value'])(bx.credit.value),
                assocPath(['accountsById', id, 'credit', 'base'])(bx.credit.base),
                assocPath(['accountsById', id, 'debit', 'value'])(bx.debit.value),
                assocPath(['accountsById', id, 'debit', 'base'])(bx.debit.base)
              ]
            }))($tate)
          return patched
        })

        const { accountsById, _status, _bxsLastFetch } = patched
        store
          .persistAppState({
            accountsById,
            _status,
            _bxsLastFetch,
            consensus: patched.consensus
          })
          .then(() =>
            p('[fetchAccountsBxs :: action :: creditors]',
              'new app state persisted'))
          .catch(err => p(err))
      })
  }

  function maybeFetchBalances (state, newConsensus, accountType = '') {
    const { accountsById = {}, _bxsLastFetch, _status } = state
    const currentBlock =
      newConsensus ? newConsensus.block : state.consensus.block
    const accounts =
      Object
        .values(accountsById)
        .filter(account =>
          accountType
            ? (account.type === accountType) && (account._lastFetch.blocks[1] < currentBlock)
            : account.type && (account._lastFetch.blocks[1] < currentBlock))
    const lastFetch =
      accounts.reduce((acc, account) =>
        account._lastFetch.blocks[1] < acc ? account._lastFetch.blocks[1] : acc, currentBlock)

    return [[
      accounts.length > 0,
      _status === 'idle',
      (_bxsLastFetch < currentBlock && lastFetch < currentBlock)
    ]].filter(c => c.every(Boolean)).map(() => {
      p('[maybeFetchBalances :: action :: creditors]',
        'fetch balances for accounts of type', accountType)
      return fetchBalances(accounts, state, newConsensus || state.consensus)
    })
  }

  return { maybeFetchBalances }
}
