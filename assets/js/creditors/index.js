import { createElement as el } from '../util/view'
import { PageLayout } from '../layout'
import { Spinner, Title } from '../common'

import { actions } from './actions'
import { Service } from './service'
import { AccountsList } from './accountsList'
import { CreditorsPage, EntitlementsPage } from '../util/router'

export const creditors = {
  actions,
  service: Service()
}

export function Creditors ({ actions, state }) {
  return (
    el(PageLayout, {},
      el(Title, {}, titleFromPageId(state.pageId)),
      (state._status === 'fetching' ||
        state._bxsLastFetch === 0 ||
        state._bxsLastFetch < state.consensus.block)
        ? el(Spinner, {}, 'Fetching creditor entitlements')
        : el(AccountsList, { state, accountType: getAccountType(state.pageId) })
    )
  )
}

function getAccountType (pageId) {
  const typeMap = {
    [CreditorsPage]: 'creditor',
    [EntitlementsPage]: 'creditor-entitlement'
  }
  return typeMap[pageId]
}

function titleFromPageId (pageId) {
  const titleMap = {
    [CreditorsPage]: 'Sales Partner',
    [EntitlementsPage]: 'Sales Partner Provisionsansprüche'
  }
  return titleMap[pageId]
}
