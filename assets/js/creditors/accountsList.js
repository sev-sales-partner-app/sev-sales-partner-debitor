import { furyTo$ } from '../util/furyNumbers'
import { AccountPage, getUrl } from '../util/router'
import { createElement as el } from '../util/view'
import {
  Table,
  Thead, TheadRow, TheadCell,
  Tbody, TbodyRow, TbodyCell,
  Tfoot, TfootRow, TfootCell,
  EmptyRow
} from '../common/table'

export function AccountsList ({ state, accountType }) {
  const { accountsById = {} } = state
  const accounts = Object
    .values(accountsById)
    .filter(account =>
      accountType
        ? account.type === accountType
        : account.type)

  return (
    el(Table, null,
      el(Thead, null,
        el(TheadRow, null,
          el(TheadCell,
            { colSpan: '4',
              label: (accountType === 'creditor-entitlement')
                ? 'Provisionsanspruch'
                : 'Account Balance'
            }
          )
        )
      ),

      el(Tbody, null,
        accounts.length === 0
          ? el(EmptyRow, { colSpan: '4' },
            'No accounts to display')
          : accounts.map((account, i) =>
            el(TbodyRow,
              { key: i + '::' + account.extid,
                onClick: () =>
                  window.location.assign(getUrl(AccountPage, {
                    address: account.address,
                    ledger: account.ledger
                  }))
              },
              el(TbodyCell,
                { label: 'Sales Partner Id' },
                account.extid && account.extid.split('::')[0]),
              el(TbodyCell,
                { label: (accountType === 'creditor-entitlement')
                  ? 'Entitlement Address'
                  : 'Account Address' },
                account.address),
              el(TbodyCell,
                { align: 'right',
                  isnegative: (account.credit.value - account.debit.value) < 0
                    ? 'true' : undefined
                },
                ((account.credit.value - account.debit.value) < 0 ? '-' : '') +
                  furyTo$.string(account.credit.value - account.debit.value)
              )
            )
          )
      ),

      el(Tfoot, null,
        el(TfootRow, null,
          el(TfootCell,
            { colSpan: '4',
              isnegative: '', // todo
              label: 'Summe' },
            furyTo$.string(accounts.reduce((acc, account) => {
              const balance = account.credit.value - account.debit.value
              return acc + balance
            }, 0))
          )
        )
      )
    )
  )
}
