import { p } from '../util/debug'
import { CreditorsPage, EntitlementsPage } from '../util/router'

import furyRpc from '../data/furyRpc'
import localStore from '../data/localStore'

import { fetchConsensus } from '../root'

const isCreditorsOrEntitlementsPage = (pageId) =>
  (pageId === CreditorsPage) || (pageId === EntitlementsPage)

const isIdle = (status) => (status === 'idle')

const accountTypeFromPageId = (pageId = CreditorsPage) => {
  const typeMap = {
    [CreditorsPage]: 'creditor',
    [EntitlementsPage]: 'creditor-entitlement'
  }
  return typeMap[pageId]
}

export const Service = (fury = furyRpc, store = localStore) => {
  function creditorsService (state, update, actions) {
    return [[
      isCreditorsOrEntitlementsPage(state.pageId), // only on entitlements page and creditors
      isIdle(state._status) // don't re-fetch when not idle
    ]].filter(c => c.every(Boolean)).map(() => {
      fetchConsensus(state, fury, store)
        .then((consensus) => {
          p('[service :: creditors]', 'consensus', consensus.block)
          actions.maybeFetchBalances(state, consensus, accountTypeFromPageId(state.pageId))
        })
        .catch(console.error)
    })
  }

  return creditorsService
}
