import { computeCreditorContext } from './compute'
import { Service } from './service'
import { Actions } from './actions'

export const settlement = {
  actions: Actions,
  computeState: computeCreditorContext,
  service: Service()
}

export { Settlement } from './view'
