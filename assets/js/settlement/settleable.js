import { furyTo$ } from '../util/furyNumbers'
import { createElement as el, b as css, theme } from '../util/view'
import {
  Table,
  Tbody,
  TbodyCell
} from '../common/table'

export function Settleable ({ state }) {
  const { settlementContext, accountsById } = state
  const {
    creditorId,
    settleableNet,
    settleableVat,
    settleableTotal,
    rxValue
  } = settlementContext
  const rxValueOk = (settleableTotal - rxValue) >= 0
  const creditorAccount = accountsById[creditorId] || {}

  return el('div',
    { className: css`
        margin-top: ${theme.space[4]}`['class'] },
    el('h5',
      { className: css(`color: ${rxValueOk ? theme.colors.green : theme.colors.darkRed}`)['class'] },
      rxValueOk
        ? 'Kontrolle Provisionsanspruch'
        : 'Rechnungsbetrag höher als Provisionsanspruch'
    ),
    el(Table,
      { className: css`
          color: ${theme.colors.black70};
          background-color: ${rxValueOk ? theme.colors.lightGreen : theme.colors.lightRed};`['class']
      },

      el(Tbody, null,
        el('tr', null,
          el(TbodyCell,
            { label: 'Sales Partner Id' },
            creditorAccount['extid']
          ),

          el(TbodyCell,
            { colSpan: '3', label: 'Sales Partner Address' },
            creditorAccount['address']
          )
        ),

        el('tr', null,
          el(TbodyCell,
            { label: 'Zeitpunkt' },
            new Date().toLocaleString('de-DE')
          ),

          el(TbodyCell,
            { label: 'offener Provisionsanspruch' },
            furyTo$.string(settleableNet)
          ),

          el(TbodyCell,
            { label: 'USt. 19 %' },
            furyTo$.string(settleableVat)
          ),

          el(TbodyCell,
            { isnegative: settleableTotal < 0 ? 'true' : '',
              label: 'Abrechnungsfähiger Gesamtbetrag' },
            furyTo$.string(settleableTotal)
          )
        )
      )
    )
  )
}
