import { getUrl, HomePage } from '../util/router'
import { createElement as el, Fragment, b as css, theme } from '../util/view'
import { Button } from '../common'
import { isInvoic, isPayableFor } from '../common/helperFns'

export function SettlementInteractions ({ accountId, actions, state }) {
  const { settlementContext, accountsById, transactionsByAccount, params } = state
  const { msgId } = params
  const { settleableTotal } = settlementContext
  const settlementAccount = accountsById[accountId] || {}
  const settlements = transactionsByAccount[accountId] || []
  const settlement = settlements.find(x => x['msg'] === msgId)
  const isPayableRx = isPayableFor(settlementAccount.address)(settlement)
  const isInvoicRx = isInvoic(settlement.data)
  const rxValueOk = (settleableTotal - settlement.value) >= 0

  return (
    el('div',
      { className: css`
          margin-top: ${theme.space[4]}`['class'] },

      settlementAccount.message
        ? el('div', null,
          el('span',
            { className: css`
                font-size: ${theme.fontSizes[2]};
                font-weight: 600;`['class'] },
            settlementAccount.message))
        : el(Fragment, null,
          el(Button,
            { onClick: () => {
              window.history.length > 1
                ? window.history.back()
                : window.location.assign(getUrl(HomePage))
            } },
            'Zurück'
          ),

          (isPayableRx && isInvoicRx) && (rxValueOk) &&
            el(Button,
              { disabled: settlementAccount.message ? 'disabled' : '',
                color: 'danger',
                css: `margin-left: ${theme.space[3]}`,
                onClick: () => actions
                  .clearSettlement(
                    accountId,
                    settlementContext.entitlementId,
                    settlement,
                    state) },
              'Mark as paid!'
            )
        )
    )
  )
}
