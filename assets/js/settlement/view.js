import { createElement as el } from '../util/view'
import { SettlementsPage } from '../util/router'
import { PageLayout } from '../layout'
import { Title, Spinner } from '../common'

import { SettlementDetail } from './detail'
import { Settleable } from './settleable'
import { SettlementInteractions } from './interactions'

export function Settlement ({ actions, state }) {
  const {
    accountsById = {},
    transactionsByAccount,
    params = {},
    settlementContext
  } = state
  const { msgId } = params
  const [ settlementAccountId, settlementAccount ] =
    Object.entries(accountsById)
      .find((kvp = [ '', {} ]) => kvp[1].ref === SettlementsPage)
  const settlements = transactionsByAccount[settlementAccountId] || []
  const settlement = settlements.find(settlement => settlement['msg'] === msgId)
  const isFetching = settlementAccount._status !== 'idle'

  return el(PageLayout, null,
    el(Title, null,
      'Settlement ', msgId || ''),
    (!settlement || isFetching || state._bxsLastFetch < state.consensus.block)
      ? el(Spinner, null, 'Looking for settlement data')
      : el('div', {},
        el(SettlementDetail,
          { actions,
            state,
            accountId: settlementAccountId,
            msgId }
        ),

        settlementContext.entitlementId &&
          el(Settleable, { state }),

        el(SettlementInteractions, { accountId: settlementAccountId, actions, state })
      )
  )
}
