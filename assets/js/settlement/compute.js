import { assoc, defaultTo, pipe } from '../util/fp'
import { SettlementsPage, SettlementPage, UserAccountPage } from '../util/router'
import { calcBalance, isPayableFor, isInvoic } from '../common/helperFns'
import { $toFury, furyTo$ } from '../util/furyNumbers'
import { netToGross } from '../util'

function getDebtorAccount (accountsById = {}, ref = UserAccountPage) {
  return Object.entries(accountsById).find(kvp => kvp[1].ref === ref)
}
// todo redundant
function getSettlementAccount (accountsById = {}, ref = SettlementsPage) {
  return Object.entries(accountsById).find(kvp => kvp[1].ref === ref)
}

function getSettlement (msgid = '', settlements = []) {
  return settlements.find(x => x.msg === msgid)
}

function getCreditorAddress (debtorAddress = '', settlement = {}) {
  const { sender, recipient } = settlement
  const senderOrRecipient = sender === debtorAddress ? recipient : sender
  return (isInvoic(settlement.data) && isPayableFor(debtorAddress)(settlement)) && senderOrRecipient
}

function getCreditorAccountId (debtorId, settlement = {}) {
  const [ debtorLedger, debtorAddress ] = debtorId.split('::')
  const creditorAddress =
    debtorAddress && getCreditorAddress(debtorAddress, settlement)
  return (creditorAddress && debtorLedger)
    ? [ debtorLedger, creditorAddress ].join('::')
    : void 0
}

function getEntitlementAccount (creditorId, accountsById) {
  const creditor = accountsById[creditorId] || {}

  return Object.entries(accountsById).find(kvp => {
    const extid = typeof kvp[1].extid === 'string' && kvp[1].extid.split('::')[0]
    return kvp[1].type === 'creditor-entitlement' && extid === creditor.extid
  })
}

export function computeCreditorContext (state) {
  const { pageId, params, accountsById, transactionsByAccount } = state

  if (pageId !== SettlementPage) return x => x

  const { msgId } = params
  const [ debtorAccountId ] = getDebtorAccount(accountsById)
  const [ settlementAccountId ] = getSettlementAccount(accountsById)
  const settlement = getSettlement(msgId, transactionsByAccount[settlementAccountId]) || {}
  const creditorAccountId = getCreditorAccountId(debtorAccountId, settlement)
  const [ entitlementAccountId, entitlementAccount ] =
    getEntitlementAccount(creditorAccountId, accountsById) || [ ]

  const entitlement =
    pipe(
      defaultTo({}), calcBalance, x => x['value'], furyTo$, $toFury
    )(entitlementAccount)
  const entitlementTotal =
    Math.round(netToGross(entitlement))
  const entitlementVat = entitlementTotal - entitlement

  return assoc('settlementContext')({
    creditorId: creditorAccountId || '',
    entitlementId: entitlementAccountId || '',
    settleableNet: entitlement,
    settleableVat: entitlementVat,
    settleableTotal: entitlementTotal,
    rxValue: settlement.value ? parseInt(settlement.value, 16) : null
  })
}
