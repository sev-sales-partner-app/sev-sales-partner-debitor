import { createElement as el, Fragment, b as css, theme } from '../util/view'
import { furyTo$ } from '../util/furyNumbers'

import {
  fromOrSender,
  toOrRecipient,
  isInvoic,
  isPayableFor
} from '../common/helperFns'

const { fontSizes, space, colors } = theme

function inline (element = 'div', style = {}) {
  return function Component (props) {
    return el(
      element,
      Object.assign({}, props, { className: style['class'] })
    )
  }
}

const Section = inline('section',
  css`
      margin-top: ${space[5]};
      max-width: 48rem;
      overflow-x: hidden;`)

const Table = inline('table', css`
      display: block;
      font-family: Courier Next,courier,monospace;
      font-size: ${fontSizes[1]}
    `['$notSmall']`
      font-size: ${fontSizes[2]}
    `['$large']`
      border-collapse: collapse;
      border-spacing: 0;
      display: table;
      table-layout: auto`)

const Details = inline('tbody', css`
    display: block;
  `['$large']`
    display: table-row-group;`)

const Vat = inline('tbody', css`
  display: block;
  border-top: 1px solid ${colors.midGray}
  margin: ${space[2]} 0 ${space[2]} 0
`['$large']`
  display: table-row-group;`)

const Total = inline('tbody', css`
  display: block;
  border-top: 1px solid ${colors.midGray};
`['$large']`
  display: table-row-group;`)

const Row = inline('tr', css`
  display: block;
`['$large']`
  display: table-row;`)

const Key = inline('td', css`
  display: block;
  padding: ${space[1]} ${space[2]} 0 ${space[2]}
`['$medium']`
  font-size: ${fontSizes[1]}
`['$large']`
  display: table-cell;
  padding: ${space[1]} ${space[2]}`)

const Key2 = inline('td', css`
  display: inline-block;
  padding: ${space[2]}
  width: 50%
`['$large']`
  display: table-cell;
  padding: ${space[1]} ${space[2]}
  width: auto`)

const Value = inline('td', css`
  display: block;
  padding: 0 ${space[2]} ${space[1]} ${space[2]}
`['$large']`
  display: table-cell;
  padding: ${space[1]} ${space[2]};`)

const Value2 = inline('td', css`
  display: inline-block;
  text-align: right;
  padding: ${space[2]}
  width: 50%
`['$large']`
  display: table-cell;
  padding: ${space[1]} ${space[2]}
    width: auto`)

export function SettlementDetail ({ state, accountId }) {
  const { accountsById, transactionsByAccount, params } = state
  const { msgId = '' } = params
  const account = accountsById[accountId] || {}
  const settlements = transactionsByAccount[accountId] || []
  const settlement = settlements.find(x => x['msg'] === msgId)
  const rxMessage = settlement.message || {}
  const isPayableRx = isPayableFor(account.address)(settlement)
  const isInvoicRx = isInvoic(settlement.data)
  const toOrRecipientRx = toOrRecipient(settlement)
  const fromOrSenderRx = fromOrSender(settlement)
  const rxMessageText = ((rxMessage) => {
    try {
      return rxMessage && rxMessage.text ? JSON.parse(rxMessage.text) : void 0
    } catch (e) {
      console.info('[SettlementDetail :: component] Settlement Message text (rx.message.text) is not an object.')
      return void 0
    }
  })(rxMessage)
  // const creditorAddress = (isPayableRx && isInvoicRx) ? settlement.sender : settlement.recipient

  return (
    el(Section, null,
      el(Table, null,
        el(Details, null,
          (isPayableRx &&
            settlement[fromOrSenderRx].toLowerCase() !== account.address.toLowerCase()) ||
          (!isPayableRx &&
            settlement[toOrRecipientRx].toLowerCase() === account.address.toLowerCase())
            ? el(Row, null,
              el(Key, null, 'Sender'),
              el(Value, null, settlement[ fromOrSenderRx ]))
            : el(Row, null,
              el(Key, null, 'Recipient'),
              el(Value, null, settlement[ toOrRecipientRx ])),

          el(Row, null,
            el(Key, null, 'Type'),
            el(Value, null,
              isInvoicRx
                ? `invoic (${isPayableRx ? 'payable' : 'receivable'})`
                : `remadv (${isPayableRx ? 'payable' : 'receivable'})`
            )
          ),

          el(Row, null,
            el(Key, null, 'Block'),
            el(Value, null, [
              settlement.blockNumber,
              '@',
              new Date(settlement['blockTime'] * 1000)
                .toLocaleString('de-DE')
            ].join(' '))
          ),

          (rxMessageText)
            ? el(Fragment, null,
              el(Row, null,
                el(Key, null, 'Rechnungsdatum'),
                el(Value, null, rxMessageText['rxDate'] || 'n / a')
              ),
              el(Row, null,
                el(Key, null, 'Rechnungsnummer'),
                el(Value, null, rxMessageText['rxNumber'] || 'n / a')
              )
            )
            : el(Row, null,
              el(Key, null, 'Text'),
              el(Value, null, rxMessage['text'] || 'n / a')
            )
        ),

        el(Vat, null,
          (settlement.base > 0) && el(Row, null,
            el(Key2, null, 'base'),
            el(Value2, null,
              (settlement.base > 0)
                ? furyTo$.string(Number.parseInt(settlement.base, 16))
                : 'n / a'
            )
          ),

          (rxMessageText && rxMessageText['Ust']) && el(Row, null,
            el(Key2, null,
              'USt. ', (rxMessageText['Ust'] * 100), ' %'),
            el(Value2, null,
              furyTo$
                .string(
                  Number.parseInt(settlement.value, 16) -
                  Number.parseInt(settlement.base, 16))
            )
          )
        ),

        el(Total, null,
          el(Row, null,
            el(Key2, null, 'value'),
            el(Value2, null, furyTo$.string(Number.parseInt(settlement.value, 16)))
          )
        )
      )
    )
  )
}
