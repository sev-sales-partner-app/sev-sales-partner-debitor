import furyRpc from '../data/furyRpc'
import localStore from '../data/localStore'
import { SettlementsPage, SettlementPage } from '../util/router'
import { fetchConsensus } from '../root'
import { p } from '../util/debug'

function isSettlementPage (pageId) {
  return pageId === SettlementPage
}

const isIdle = (status) => (status === 'idle')

export const Service = (fury = furyRpc, store = localStore) => {
  // let triggerd = false
  function settlementService (state, update, actions) {
    const { pageId, params, accountsById, transactionsByAccount } = state
    const { msgId } = params
    const account = Object.values(accountsById).find(account =>
      Boolean((account.ref === SettlementsPage))
    ) || {}
    const accountId = [ account.ledger, account.address ].join('::')
    const transactions = transactionsByAccount[accountId]
    const settlement = transactions && transactions.find(tx => tx.msg === msgId)

    if (!isSettlementPage(pageId)) return void 0
    else if (
      // !triggerd &&
      isIdle(state._status) &&
      (state._bxsLastFetch < state.consensus.block)
    ) {
      // triggerd = true
      return actions.maybeFetchBalances(state, state.consensus, 'creditor-entitlement')
    } else {
      return [[
        isSettlementPage(pageId),
        isIdle(state._status),
        isIdle(account._status),
        account._status !== 'fetching_more',
        account._status !== 'fetching',
        msgId != null,
        settlement == null,
        transactions == null ||
        account._lastFetch == null ||
        account._lastFetch.blocks[0] > 0
      ]].filter(c => c.every(Boolean))
        .map(() => {
          fetchConsensus(state, fury, store)
            .then((consensus) => {
              p('[service :: settlement]', 'Current consensus', consensus.block)
              p('[service :: setlement]',
                'fetch transactions for account', accountId)

              return actions.fetchAccountTxs(accountId, state, consensus, {
                ledgerType: 'transferable',
                maxBlocks: consensus.block,
                threshold:
                (transactions && transactions.length > 0)
                  ? transactions.length * 5
                  : 5
              })
            })
            // .then(() => { triggerd = false })
            .catch(console.error)
        })
    }
  }

  return settlementService
}
