import { grossToNet } from '../util'
import { p } from '../util/debug'
import furyrpc from '../data/furyRpc'
import localstore from '../data/localStore'
import { assoc, assocPath, pipe } from '../util/fp'
import { UserAccountPage } from '../util/router'
import { vat as vatAddress } from '../util/constants'

export function Actions (update, fury = furyrpc, store = localstore) {
  /// helpers
  function Clear (Node = () => {}) {
    return function clear (payload = {}) {
      return fury.addTxToLedger(Node(), payload)
    }
  }

  function reverseTx (payload = {}) {
    return Object.assign(
      {},
      payload,
      { from: payload.to, to: payload.from }
    )
  }

  function clearSettlement (
    settlementAccountId = '',
    entitlementAccountId = '',
    settlement = {},
    state = {}
  ) {
    const { accountsById, user } = state
    const settlementAccount = accountsById[settlementAccountId] || {}
    const entitlementAccount = accountsById[entitlementAccountId] || {}
    const { message } = settlement
    const Unode = () => fury.createNode(user)
    const rxNetVAlue = Math.round(grossToNet(message.value))

    function patchSettlementMessage (string = '') {
      return assocPath([ 'accountsById', settlementAccountId, 'message' ])(string)
    }

    function patchSettlementStatus (string = '') {
      return assocPath(
        ['accountsById', settlementAccountId, '_status']
      )(string)
    }

    try {
      const unode = Unode()
      if (rxNetVAlue !== message.base) throw new Error('Settlement value mismatch')
      if (
        unode.wallet.address.toLowerCase() !== settlementAccount.address.toLowerCase() ||
        unode.wallet.address.toLowerCase() !== message.from.toLowerCase()
      ) throw new Error('address mismatch')
    } catch (err) {
      console.error(err)

      update(patchSettlementMessage(err.message))

      return void 0
    }

    const clearTx = Clear(Unode)

    const tx1of3 = {
      ledger: entitlementAccount.ledger, // userAccount ledger
      from: entitlementAccount.address,
      to: message.from,
      base: rxNetVAlue,
      value: rxNetVAlue
    }

    const tx2of3 = {
      ledger: entitlementAccount.ledger,
      from: message.from,
      to: message.to,
      base: rxNetVAlue,
      value: message.value
    }

    const tx3of3 = {
      ledger: entitlementAccount.ledger,
      from: vatAddress,
      to: message.from,
      // base: message.value - rxNetVAlue,
      value: message.value - rxNetVAlue
    }

    // console.log('it is working')
    // console.log('settlement account id', settlementAccountId)
    // console.log('entitlement account id', entitlementAccountId)
    // console.log('settlementAccount', settlementAccount)
    // console.log('entitlementAccount', entitlementAccount)
    // console.log('settlement', settlement)
    // console.log('uNode.wallet.address', uNode.wallet.address)
    // console.log('vatAddress', vatAddress)
    // console.log('rxNetVAlue', rxNetVAlue)

    update(patchSettlementMessage(
      'Step 1 of 2: Deducting settlement amount from entitlement account...'))

    return clearTx(tx1of3)
      .then(txn => {
        p('transaction 1 of 3 done:', txn)
        update(patchSettlementMessage(
          'Step 2 of 2: Transferring entitlement to creditor account'))
        return clearTx(tx2of3)
      })
      .then(txn => {
        p('transaction 2 of 3 done', txn)
        update(pipe(
          patchSettlementMessage(''),
          patchSettlementStatus('idle'),
          assoc('pageId')(UserAccountPage),
          assoc('params')({})
        ))
        return clearTx(tx3of3)
      }, err => {
        console.error('Transaction 2 of 3 failed. Rolling back tx 1 of 3...')
        clearTx(reverseTx(tx1of3))
          .then(txn => {
            console.log('[clearSettlement :: action :: settlement]', 'Rollback of tx 1 successful:', txn)
          })
        throw err
      })
      .then(txn => {
        p('[clearSettlement :: action :: settlement]', 'transaction 3 of 3 done', txn)
      }, err => {
        console.error('[clearSettlement :: action :: settlement]', 'Transaction 3 of 3 failed. Rolling back tx 1 and 2 of 3...')
        clearTx(reverseTx(tx2of3))
          .then(txn => {
            console.log('[clearSettlement :: action :: settlement]', 'rollback 2 of 3 successful:', txn)
            return clearTx(reverseTx(tx1of3))
          })
          .then(txn => console.log('[clearSettlement :: action :: settlement]', 'rollback 1 of 3 successful:', txn))
        throw err
      })
      .catch(err => {
        console.error(err)
        update(pipe(
          patchSettlementMessage(''),
          patchSettlementStatus('idle')
        ))
      })
  }

  return { clearSettlement }
}
