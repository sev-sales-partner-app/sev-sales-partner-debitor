import axios from 'axios'
import { apiHost } from '../util/constants'

/**
 *
 * @type {AxiosInstance} agentApi
 */
const agentApi = axios.create({
  baseURL: apiHost,
  timeout: 9000
})

/**
 *
 * @param token
 * @return {AxiosPromise<any>}
 */
function checkAuth (token) {
  return agentApi.get('/auth/check', {
    headers: { 'Authorization': token }
  })
}

/**
 *
 * @param token
 * @return {AxiosPromise<any>}
 */
function perpetuateAuth (token) {
  return agentApi.get('/auth/perpetuation', {
    headers: { 'Authorization': token }
  })
}

/**
 *
 * @param {String} username
 * @param {String} callbackUrl
 * @param {String} role
 * @return {AxiosPromise<any>}
 */
function submitCredentials ({
  username,
  callbackUrl,
  role = 'debtor'
}) { return agentApi.post('/auth', { username, callbackUrl, role }) }

/**
 *
 * @param token
 * @param salesPartnerAddress
 * @return {AxiosPromise<any>}
 */
function fetchSalesPartnerId (token, salesPartnerAddress) {
  // console.debug(token, salesPartnerAddress)
  return (
    agentApi.get(`/auth/${salesPartnerAddress}`, {
      headers: { 'Authorization': token }
    })
  )
}

/**
 *
 * @param formData
 * @param token
 * @return {AxiosPromise<any>}
 */
function submitSettlement (formData, token) {
  return agentApi.put('/settlement', formData, {
    headers: {
      'Authorization': token,
      'Content-Type': 'multipart/form-data'
    }
  })
}

/**
 * returns array of creditor objects
 *
 * @param token
 * @return {AxiosPromise<any>}
 */
export function getCreditors (token) {
  return agentApi.get('/creditors', {
    headers: { 'Authorization': token }
  })
}

/**
 *
 * @param {Error} err
 */
const logError = (err) => {
  if (err['response']) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.error(err['response'].data)
    console.error(err['response'].status)
    console.error(err['response'].headers)
  } else if (err['request']) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.error(err['request'])
  } else {
    // Something happened in setting up the request that triggered an Error
    console.error('Error', err.message)
  }
  console.error(err)
  console.log(err['config'])
}

export default {
  checkAuth,
  perpetuateAuth,
  submitCredentials,
  submitSettlement,
  fetchSalesPartnerId,
  getCreditors,
  logError
}
