import { mergeDeepLeft } from '../util/fp'
import furyRpc from './furyRpc'

// _getItem :: String -> Promise<any>
function _getItem (key) {
  return new Promise((resolve, reject) => {
    try {
      const string = window.localStorage.getItem(key)
      resolve(string && JSON.parse(string))
    } catch (err) {
      reject(err)
    }
  })
}

// _setItem :: String -> {k: v} -> Promise<any>
function _setItem (key) {
  return function (data) {
    return new Promise((resolve, reject) => {
      try {
        const string = JSON.stringify(data)
        window.localStorage.setItem(key, string)
        resolve(string)
      } catch (err) {
        reject(err)
      }
    })
  }
}

// function _clearAll () {
//   return new Promise((resolve, reject) => {
//     try {
//       resolve(window.localStorage.clear())
//     } catch (e) {
//       reject(e)
//     }
//   })
// }

// Clears local store

function clearAppState () {
  return _setItem('App::state')({})
}

// manage block number and block time in local store

function fetchBlockTime (blockNumber) {
  return _getItem('block::' + blockNumber)
}

function setBlockTime (blockNumber, ts) {
  return _setItem('block::' + blockNumber)(ts)
}

function matchBlockTime (block, frpc = furyRpc) {
  return fetchBlockTime(block)
    .then(time => (time != null)
      ? ({ block, time })
      : frpc.fetchBlockTimestamp(block)
        .then(rpcTime => {
          setBlockTime(block, rpcTime)
          return { block, time: rpcTime }
        })
    )
    .catch(err =>
      console.error('[matchBlockTime :: localStore] error', err))
}

// manage app state in local store

function fetchPersistedState () {
  return _getItem('App::state')
}

function persistAppState (state) {
  return _getItem('App::state')
    .then(mergeDeepLeft(state))
    .then(_setItem('App::state'))
}

export default {
  // clearAll: _clearAll,
  clearAppState,
  matchBlockTime,
  fetchPersistedState,
  persistAppState
}
