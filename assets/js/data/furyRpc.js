const axios = require('axios')
const settings = require('../util/constants')
const OxOO = '0x0000000000000000000000000000000000000000'

const f = document.StromDAOBO

function createNode ({ extid = 'default', privateKey } = {}) {
  return new f.Node({
    external_id: extid,
    privateKey,
    testMode: true,
    rpc: settings.rpcHost,
    abilocation: settings.abiLocation
  })
}
exports.createNode = createNode

exports.createAccount = function (username, password) {
  console.debug('[createAccount :: furyRpc]', 'fn called with -u ', username, ' and -p ', password)
  return new f.Account(username, password)
}
exports.utils = new f.Util()

function createStringStore (n, str) {
  return n
    .stringstoragefactory()
    .then(ssf => ssf.build(str))
}
exports.createStringStore = createStringStore

function fetchStringStore (address, n = createNode()) {
  return n
    .stringstorage(address)
    .then(sst => sst.str())
}
exports.fetchStringStore = fetchStringStore

function getRelation (address, key, n = createNode()) {
  return n
    .roleLookup()
    .then(rl => rl.relations(address, key))
    .then(res => {
      console.debug('[getRelation :: furyRpc]', `relation key ${key} of ${address} points to ${res}`)
      return res
    })
}
exports.getRelation = getRelation

function setRelation (n, key, toAddress, overwriteExisting) {
  return n
    .roleLookup()
    .then(rl => rl.relations(n.wallet.address, key))
    .then(r => {
      if (r === OxOO || overwriteExisting === true) {
        console.debug('[setRelation :: furyRpc]', `setting role lookup relation key ${key} of ${n.wallet.address} to  ${toAddress}`)
        return n
          .roleLookup()
          .then(rl => rl.setRelation(key, toAddress))
      }
      console.warn('[setRelation :: furyRpc]', `relation key ${key} already exists: ${r}`)
      console.debug('[setRelation :: furyRpc]', 'will not overwrite existing key')
    })
}
exports.setRelation = setRelation

exports.readRelationString = function (address, key, n = createNode()) {
  return getRelation(address, key, n)
    .then(a => {
      return a === OxOO
        ? console.error(
          '[readRelationString :: furyRpc]',
          `no string storage contract found under role lookup key ${key} for ${address}`)
        : fetchStringStore(a, n) // n.stringstorage(a).then(sst => sst.str())
    })
}

exports.writeRelationString = function (n, key, string) {
  return n
    .stringstoragefactory()
    .then(ssf => {
      console.debug(
        '[writeRelationString :: furyRpc]',
        `role lookup key ${key} for ${n.wallet.address} will point to new stringStore contract with content: ${string}`)
      return ssf.buildAndAssign(key, string)
    })
}

exports.fetchMeterpointReading = function (address, n = createNode()) {
  return n
    .mpr()
    .then(mpr => mpr.readings(address))
}

exports.storeMeterpointReading = function (n, uint256) {
  return n
    .mpr()
    .then(mpr => mpr.storeReading(uint256))
}

function createLedger (n, overwriteExisting) {
  console.debug('[createLedger :: furyRpc]', `creating ledger for ${n.wallet.address}...`)
  return n
    .stromkontoproxyfactory()
    .then(skpf => skpf.build())
    .then(lA => {
      return setRelation(n, 42, lA, overwriteExisting)
        .then((res) => {
          if (res == null) {
            console.warn(
              '[createLedger :: furyRpc]',
              `new ledger with address ${lA} created but could not be stored under roleLookup key 42 of 
              ${n.wallet.address}`
            )

            throw new Error()
          }

          console.debug(
            '[createLedger :: furyRpc]',
            `new ledger with address ${lA} created and stored under roleLookup key 42 of ${n.wallet.address}`
          )
          return lA
        })
    }
    )
}
exports.createLedger = createLedger

exports.fetchLedger = function (n, createNew = true) {
  return getRelation(n.wallet.address, 42, n)
    .then(lA => (lA === OxOO && createNew)
      ? createLedger(n)
      : lA
    )
}

exports.fetchLedgerOwner = function (ledgerAddress, {
  n = createNode(),
  ledgertype = 'stromkontoproxy'
}) {
  return n[ledgertype](ledgerAddress)
    .then(ledger => ledger.owner())
    .then(arr => arr[0]) // // result array only has one item // TODO verify always true
}

exports.addSenderToLedger = function (n, senderAddress, ledgerAddress) {
  return n
    .stromkontoproxy(ledgerAddress)
    .then(ledger => ledger.modifySender(senderAddress, true))
    .then((tx) => {
      console.debug('[addSenderToLedger :: furyRpc]',
        `added address ${senderAddress} as sender to ledger ${ledgerAddress} (tx hash: ${tx})`)
      return tx
    })
}

const fetchAccountCredit = function (ledgerAddress, accountAddress, {
  n = createNode(),
  ledgerType = 'stromkonto' // || 'transferable'
}) {
  return n[ledgerType](ledgerAddress)
    .then(l => Promise.all([
      l.balancesHaben(accountAddress),
      l.baseHaben(accountAddress)
    ]))
    .then(arr => ({
      value: arr[0],
      base: arr[1]
    }))
}
exports.fetchAccountCredit = fetchAccountCredit

function fetchAccountDebit (ledgerAddress, accountAddress, {
  n = createNode(),
  ledgerType = 'stromkonto' // || 'transferable'
}) {
  return n[ledgerType](ledgerAddress)
    .then(l => Promise.all([
      l.balancesSoll(accountAddress),
      l.baseSoll(accountAddress)
    ]))
    .then(arr => ({
      value: arr[0],
      base: arr[1]
    }))
}
exports.fetchAccountDebit = fetchAccountDebit

exports.fetchAccountBalance = function (ledgerAddress, accountAddress, {
  n = createNode(),
  ledgerType = 'stromkonto' // || 'transferable'
}) {
  return Promise.all([
    fetchAccountCredit(ledgerAddress, accountAddress, { n, ledgerType }),
    fetchAccountDebit(ledgerAddress, accountAddress, { n, ledgerType })
  ])
    .then(arr => ({
      address: accountAddress,
      ledger: ledgerAddress,
      credit: arr[0],
      debit: arr[1],
      balance: {
        value: arr[0].value - arr[1].value,
        base: arr[0].base - arr[1].base
      }
    }))
}

function fetchTxHistory (ledgerAddress, accountAddress, blocks = 1000, {
  n = createNode(),
  ledgerType = 'stromkonto' // || 'transferable'
}) {
  return n
    .rpcprovider
    .getBlockNumber()
    .then(cblk => n[ledgerType](ledgerAddress)
      .then(l => l.history(accountAddress, blocks <= cblk ? blocks : cblk))
      .then(history => {
        return {
          query: [cblk - (blocks <= cblk ? blocks : cblk), cblk],
          data: history
        }
      })
    )
}
exports.fetchTxHistory = fetchTxHistory

function fetchCurrentBlockNumber (n = createNode()) {
  return n
    .rpcprovider
    .getBlockNumber()
}
exports.fetchCurrentBlockNumber = fetchCurrentBlockNumber

exports.fetchDeepTxHistory = function (ledgerAddress, accountAddress, {
  threshold = 1,
  initialBlocks = 10000,
  maxBlocks,
  n = createNode(),
  ledgerType = 'stromkonto' // || 'transferable'
}) {
  if (ledgerAddress == null || accountAddress == null) {
    return Promise.reject(new Error('[fetchDeepTxHistory :: furyRpc]' + ' leger or account address not defined'))
  }

  let c = 0
  function drillDeep (l, a, t, b, maxb) {
    c += 1
    console.debug('[drillDeep :: furyRpc]', 'round:', c, 'with args:', l, a, t, b, maxb, ledgerType)
    return fetchTxHistory(l, a, b, { n, ledgerType })
      .then(o => {
        return (o.data.length < t && b < maxb)
          ? drillDeep(l, a, t, (b + b > maxb) ? maxb : b + b, maxb)
          : o
      })
  }
  return n
    .rpcprovider
    .getBlockNumber()
    .then(cblk => {
      if (!maxBlocks) maxBlocks = cblk
      if (initialBlocks > maxBlocks) initialBlocks = maxBlocks
      return drillDeep(
        ledgerAddress,
        accountAddress,
        threshold,
        (initialBlocks < cblk) ? initialBlocks : cblk,
        (maxBlocks < cblk) ? maxBlocks : cblk
      )
    })
}

function addTxToLedger (n, {
  ledger,
  from,
  to,
  base = 0,
  value
}) {
  return n
    .stromkonto(ledger)
    .then(l => l.addTx(from, to, value, base))
}
exports.addTxToLedger = addTxToLedger

function sendRxToRecipient (n, {
  ledger = '0x5856b2AE31ed0FCf82F02a4090502DC5CCEec93E', // 0x5856b2AE31ed0FCf82F02a4090502DC5CCEec93E
  recipient,
  msgAddress,
  base = 0,
  value,
  isPayable = false
}) {
  return n
    .transferable(ledger)
    .then(l => l.addRx(recipient, msgAddress, value, base, isPayable))
}
exports.sendRxToRecipient = sendRxToRecipient

exports.addTxWithRx = function (n, {
  ledger,
  rxLedger,
  from,
  to,
  base,
  value,
  text,
  isPayable = false
}) {
  let res = {}
  const tx = { ledger, from, to, base, value }
  const msg = window.btoa(JSON.stringify(Object.assign({}, { from, to, base, value }, text ? { text } : {})))
  return addTxToLedger(n, tx)
    .then(txh => {
      console.debug('[addTxWithRx :: furyRpc]', `added Tx (${txh}) to ledger ${ledger}`)
      res.txHash = txh
      return createStringStore(n, msg)
    })
    .then(msgAddress => {
      console.debug('[furyRpc :: addTxWithRx]', `created Rx message @ address ${msgAddress} with content: ${msg}`)
      res.msgAddress = msgAddress
      return sendRxToRecipient(n, {
        ledger: rxLedger,
        recipient: isPayable ? to : from, // note invoic switch
        msgAddress,
        base,
        value,
        isPayable
      })
    })
    .then(rxh => {
      console.debug('[addTxWithRx :: furyRpc]', `submitted Rx type ${isPayable ? 'remadv' : 'incoic'} to settlement ledger ${rxLedger}`)
      res.rxHash = rxh
      return res
    })
}

exports.fetchBlockTimestamp = function (blockNumber) {
  let opts = {
    url: settings.rpcHost,
    method: 'post',
    dataType: 'json',
    headers: { 'content-type': 'application/json' },
    data: `{"jsonrpc":"2.0","method":"eth_getBlockByNumber","params":["${blockNumber}", true],"id":1}`
  }
  return Promise.resolve()
    .then(() => axios(opts))
    .then(res => { console.log('RES', res); return 0 }) // TODO FIX timestamp problem (new API)
    // .then(res => Number.parseInt(res.data.result.timestamp))
  // * 1000 // time in millisec // console.debug('[fetchBlockTimestamp]', res.data.result)
}
