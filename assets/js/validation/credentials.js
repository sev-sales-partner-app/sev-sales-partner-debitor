import isEmail from 'validator/lib/isEmail'
import isNumeric from 'validator/lib/isNumeric'
import isJWT from 'validator/lib/isJWT'
import isAlphanum from 'validator/lib/isAlphanumeric'
import jwtDecode from 'jwt-decode'

import { now } from '../util'

// helpers

// const jwtPattern = new RegExp(/^[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+\.[A-Za-z0-9-_.+/=]*$/)

const string = label => (value) => {
  return typeof value !== 'string' && { value, error: label + ' must be of type String' }
}

const required = label => value => {
  return value.length === 0 && { value, error: label + ' is  a required field!' }
}

const email = value => {
  return !isEmail(value) && { value, error: `"${value}" is not a valid E-mail address!` }
}

const jwt = value => {
  return !isJWT(value) && { value, error: `"${value}" is not a valid web token!` }
}

const alphanum = value => {
  return !isAlphanum(value) && { value, error: `"${value || 'value'}" is not alphanumeric!` }
}

const num = value => {
  return !isNumeric(typeof value !== 'string'
    ? String(value)
    : value) && { value, error: `"${value || 'value'}" is not numeric!` }
}

const has42Char = value => {
  return (value.length !== 42) && { value, error: `"${value}" must be 42 characters!` }
}

const expired = label => value => {
  return (value < now()) && { value, error: `"${label}" expired!` }
}

export function validateUsername (value) {
  return (
    string('User Name')(value) ||
    required('User Name')(value) ||
    email(value) ||
    { value, error: '' }
  )
}

export function validateToken (value) {
  return (
    string('Web token')(value) ||
    required('Web token')(value) ||
    jwt(value) ||
    { value, error: '' }
  )
}

// TODO: finalize payload validation
export function validateTokenPayload (token) {
  const payload = (() => {
    try {
      if (typeof token === 'string') return jwtDecode(token)
      if (typeof token === 'object') return token
      return { error: new Error('Token neither string nor object') }
    } catch (error) {
      return { error }
    }
  })()
  const { extid, address, key, iat, exp, aud, iss, scope } = payload
  const value = {
    extid: (string('extid')(extid) || required('extid')(extid) || email(extid) || { value: extid, error: '' }),
    address: (
      string('address')(address) ||
      required('address')(address) ||
      alphanum(address) ||
      has42Char(address) ||
      { value: address, error: '' }),
    key: (
      string('key')(key) ||
      alphanum(key) ||
      { value: key, error: '' }),
    scope: (
      (scope !== 'debtor' && { value: scope, error: 'Not authorized!' }) ||
      { value: scope, error: '' }),
    iat: (
      num(iat) || { value: iat, error: '' }),
    exp: (
      expired('Web token')(exp) ||
      { value: exp, error: '' }),
    aud: (string('aud')(aud) || { value: aud, err: '' }),
    iss: (string('iss')(iss) || { value: iss, err: '' })
  }

  const error = Object
    .values(value)
    .filter(({ value, error }) => error)
    .map(({ error }) => error)[0] || ''

  return { value, error }
}
