import { createElement as el } from '../util/view'

import { Spinner, Title } from '../common'
import { PageLayout } from '../layout'
import { Transactions } from './txsList'
import { actions } from './actions'
import { Service } from './service'

export const account = {
  actions,
  service: Service()
}

export function Account ({ actions, state }) {
  const {
    pageId,
    params,
    accountsById,
    transactionsByAccount,
    consensus
  } = state
  const { address, ledger } = params
  const fallbackAccount =
    Object.values(accountsById).find(account =>
      Boolean((account.ref === pageId))
    ) || {}
  const accountId = (ledger && address)
    ? [ ledger, address ].join('::')
    : fallbackAccount.ledger + '::' + fallbackAccount.address
  const account = accountsById[accountId] || { address, ledger }
  const accountTxs = transactionsByAccount[accountId]
  const isSettlementsPage = account.type === 'settlement'

  return (
    el(PageLayout, {},
      el(Title, null, 'Account ' + (account.extid || account.address || params.address || '')),
      (accountTxs == null ||
        account._lastFetch == null ||
        account._lastFetch.blocks[1] < consensus.block)
        ? el(Spinner, {}, 'Loading ' + (isSettlementsPage ? 'Settlements' : 'Transactions'))
        : el(Transactions, { accountId, actions, state })
    )
  )
}
