import { p } from '../util/debug'
import { AccountPage, SettlementsPage, UserAccountPage } from '../util/router'

import furyRpc from '../data/furyRpc'
import localStore from '../data/localStore'

import { fetchConsensus } from '../root'

const isAccountOrSettlementsPage = (pageId) =>
  (pageId === AccountPage) || pageId === UserAccountPage || (pageId === SettlementsPage)

const isIdle = (status) => (status === 'idle')

export const Service = (fury = furyRpc, store = localStore) => {
  function accountService (state, update, actions) {
    const { pageId, params, accountsById } = state
    const { address, ledger } = params
    const fallback =
      Object.values(accountsById).find(account =>
        Boolean((account.ref === pageId))
      ) || {}
    const accountId =
      (ledger && address)
        ? [ ledger, address ].join('::')
        : [ fallback.ledger, fallback.address ].join('::')
    const account = accountsById[accountId] || {}

    return [[
      isAccountOrSettlementsPage(state.pageId), // only on account page
      isIdle(state._status), // don't re-fetch when not idle
      account._status !== 'fetching_more',
      account._status !== 'fetching' // don't re-fetch when account not idle
    ]].filter(c => c.every(Boolean)).map(() => {
      fetchConsensus(state, fury, store)
        .then((consensus) => {
          p('[service :: account]', 'Current consensus', consensus.block)
          actions.maybeFetchTransactions(accountId, state, consensus)
        })
        .catch(console.error)
    })
  }

  return accountService
}
