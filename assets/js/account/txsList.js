import { createElement as el } from '../util/view'
import { furyTo$ } from '../util/furyNumbers'
import {
  Table,
  Thead,
  TheadRow,
  TheadCell,
  Tbody,
  EmptyRow,
  TbodyRow,
  TbodyCell,
  ShowMoreCell,
  Tfoot,
  TfootRow,
  TfootCell
} from '../common/table'
import { matchCase } from '../util/fp'
import { AccountPage, SettlementPage, getUrl } from '../util/router'

import {
  calcBalance,
  calcVBalance,
  fromOrSender,
  toOrRecipient,
  isInvoic,
  isPayableFor
} from '../common/helperFns'

function filterTransactions (filter = '', { address, type }, txs = []) {
  const equals = a => b => (a === b)

  return matchCase(filter)
    .on(equals('sent'), () =>
      txs.filter(tx =>
        tx[ fromOrSender(tx) ].toLowerCase() === address.toLowerCase()))
    .on(equals('received'), () =>
      txs.filter(tx =>
        tx[ fromOrSender(tx) ].toLowerCase() !== address.toLowerCase()))
    .otherwise(() => txs)
}

export function Transactions ({ accountId, actions, state }) {
  const { params, accountsById, transactionsByAccount } = state
  const account = accountsById[accountId]
  const accountBalance = (account.type !== 'settlement')
    ? calcBalance(account).value
    : calcBalance(account).value * -1
  const accountTransactions = transactionsByAccount[accountId]
  const filteredTxs =
    filterTransactions(params.filter, account, accountTransactions)
  const vBalance = calcVBalance(account.address, filteredTxs)
  const startBalance = accountBalance - vBalance

  return (
    el('div', null,
      el(Table, null,

        el(Thead, null,
          el(TheadRow, null,
            el(TheadCell,
              { colSpan: 4,
                isnegative: (accountBalance < 0) ? 'true' : '',
                label: 'Saldo' },
              furyTo$.string(accountBalance))
          )
        ),

        el(Tbody, null,
          filteredTxs.length === 0
            ? el(EmptyRow,
              { colSpan: 4 },
              // 'no receipts to display'
              account._status === 'loading' ? 'fetching...' : 'no txs to display')
            : filteredTxs.map((tx, i) => {
              const isPayableTx = isPayableFor(account.address)(tx)
              const toOrRecipientTx = toOrRecipient(tx)
              const fromOrSenderTx = fromOrSender(tx)

              return (
                el(TbodyRow,
                  (account.type !== 'settlement')
                    ? { key: i }
                    : {
                      key: i,
                      onClick: // x => x
                      () => window
                        .location
                        .assign(getUrl(SettlementPage, { msgId: tx.msg }))
                    },
                  el(TbodyCell,
                    { label: 'Buchungsdatum' },
                    new Date(tx['blockTime'] * 1000).toLocaleDateString('de-DE')),

                  el(TbodyCell,
                    { label: 'Type' },
                    account.type !== 'settlement'
                      ? isPayableTx
                        ? 'debit (payable)'
                        : 'credit (receivable)'
                      : isInvoic(tx.data)
                        ? `invoic (${isPayableTx ? 'payable' : 'receivable'})`
                        : `remadv (${isPayableTx ? 'payable' : 'receivable'})`
                  ),

                  (account.type !== 'settlement')
                    ? el(TbodyCell,
                      { label: isPayableTx ? 'To' : 'From' },
                      el('a',
                        { href: getUrl(AccountPage, {
                          address: isPayableTx
                            ? tx[toOrRecipientTx].toLowerCase()
                            : tx[fromOrSenderTx].toLowerCase(),
                          ledger: account.ledger }) },
                        tx[ isPayableTx ? toOrRecipientTx : fromOrSenderTx ])
                    )
                    : el(TbodyCell,
                      { label:
                        ((isPayableTx &&
                          tx[fromOrSenderTx].toLowerCase() !== account.address.toLowerCase()) ||
                        (!isPayableTx &&
                          tx[toOrRecipientTx].toLowerCase() === account.address.toLowerCase()))
                          ? 'From'
                          : 'To'
                      },
                      ((isPayableTx &&
                        tx[fromOrSenderTx].toLowerCase() !== account.address.toLowerCase()) ||
                      (!isPayableTx &&
                        tx[toOrRecipientTx].toLowerCase() === account.address.toLowerCase()))
                        ? tx[fromOrSenderTx]
                        : tx[toOrRecipientTx]
                    ),

                  el(TbodyCell,
                    { align: 'right',
                      isnegative: isPayableTx ? 'true' : '' },
                    (isPayableTx ? '-' : '') +
                    furyTo$.string(Number.parseInt(tx.value, 16))
                  )
                )
              )
            })
        ),

        el(Tbody, null,
          el(TbodyRow, null,
            el(ShowMoreCell,
              { disabled: account._status === 'fetching_more' ? 'disabled' : '',
                fromblock: String(account._lastFetch.blocks[0]),
                txscount: String(filteredTxs.length),
                onClick: () => actions.fetchAccountTxs(
                  accountId,
                  state,
                  state.consensus,
                  { threshold: accountTransactions.length * 2,
                    maxBlocks: state.consensus.block,
                    ledgerType:
                      (account.type === 'settlement')
                        ? 'transferable'
                        : 'stromkonto'
                  }
                )
              },
              account._status !== 'idle' ? 'Loading...' : 'Load More Transactions '
            )
          )
        ),

        el(Tfoot, null,
          el(TfootRow, null,
            el(TfootCell,
              { colSpan: '4',
                isnegative: (startBalance < 0) ? 'true' : '',
                label: 'Anfangssaldo' },
              furyTo$.string(startBalance))
          )
        )
      )
    )
  )
}
