import furyrpc from '../data/furyRpc'
import localstore from '../data/localStore'
import { p } from '../util/debug'
import { assoc, assocPath, pipe } from '../util/fp'
import { OxOO } from '../util/constants'
import { now } from '../util'

export function actions (update, fury = furyrpc, store = localstore) {
  // helper fns

  // isTransferable :: String -> Boolean
  function isSettlementAccount (ledgerOrAccountType) {
    return (
      ledgerOrAccountType === 'transferable' ||
      ledgerOrAccountType === 'settlement'
    )
  }

  // injectTxsBlockTimes :: ({ data: Array, query: Array }) -> Promise<{ data: Array, query: Array }>
  function injectTxsBlockTimes ({ data = [], query = [] }) {
    return Promise
      .all(data.map((tx) =>
        store.matchBlockTime(tx.blockNumber)
          .then(({ time }) => assoc('blockTime')(time)(tx))
      ))
      .then(dataArr => ({ data: dataArr, query }))
      .catch(err => {
        console.error(err); return { data, query }
      })
  }

  // handleSettlementMsgs :: Object<{ data: Array?, query: Array? }> -> Promise<{ data: Array, query: Array }>
  function handleSettlementsMsgs ({ data = [], query = [] }, node = fury.createNode()) {
    return data.length === 0
      ? { data, query }
      : Promise
        .all(data.map((tx = {}) =>
          (tx.msg == null || tx.msg === OxOO)
            ? Promise.resolve(tx)
            : fury
              .fetchStringStore(tx.msg, node)
              .then((encStrg = '') =>
                assoc('message')(JSON.parse(window.atob(encStrg)))(tx))
        ))
        .then(dataArr => ({ data: dataArr, query }))
        .catch(err => {
          console.error(err); return { data, query }
        })
  }

  // createAccount :: String -> Object
  function createAccount (id = '') {
    const arr = id.split('::')
    return {
      address: arr[1],
      ledger: arr[0],
      type: 'queried',
      debit: { value: 0, base: 0 },
      credit: { value: 0, base: 0 },
      _status: 'idle',
      _lastFetch: { blocks: [0, 0], time: 0 }
    }
  }

  function fetchAccountTxs (
    accountId = '',
    state = {},
    newConsensus,
    { ledgerType = 'stromkonto', maxBlocks, threshold, n = fury.createNode() }
  ) {
    const account = state.accountsById[accountId] || createAccount(accountId)
    const accountTxs = state.transactionsByAccount[accountId] || []
    const isFetchingMore = accountTxs.length > 0
    const [ , toBlk ] = account._lastFetch.blocks
    const max = isFetchingMore && !maxBlocks ? newConsensus.block - toBlk : maxBlocks
    const opts = {
      ledgerType,
      maxBlocks: max,
      threshold,
      n
    }

    p('[fetchAccountTxs :: action :: account]',
      'set state._status to "fetching"',
      `set account._status to "${isFetchingMore ? 'fetching_more' : 'fetching'}"`)

    update(pipe(
      assoc('_status')('fetching'), // Note: is needed for queried / unknown accounts
      assocPath(['accountsById', accountId, '_status'])(
        isFetchingMore ? 'fetching_more' : 'fetching')
    ))

    return Promise.all([
      fury.fetchAccountBalance(account.ledger, account.address, opts),
      fury.fetchDeepTxHistory(account.ledger, account.address, opts)
        .then(injectTxsBlockTimes)
        .then(
          isSettlementAccount(opts.ledgerType)
            ? x => handleSettlementsMsgs(x, opts.n)
            : x => x)
    ])
      .then(([ balance, txHistory ]) => {
        console.assert(
          account.ledger === balance.ledger && account.address === balance.address,
          '[fetchAccountTxs :: action :: account]',
          `public keys in arg ${account} and response ${balance} should be equal`)

        const patchedAccount = Object.assign({}, account, {
          credit: balance.credit,
          debit: balance.debit,
          _status: 'idle',
          _lastFetch: { time: now(), blocks: txHistory.query }
        })
        const consensus = newConsensus || state.consensus

        let filtered = isFetchingMore && !maxBlocks
          ? txHistory.data.filter(x => x.blockNumber > toBlk)
          : txHistory.data
        // console.log('filtered', filtered, 'toBlk', toBlk)
        const txs = isFetchingMore && !maxBlocks
          ? accountTxs.concat(filtered).sort((a, b) =>
            -(a.blockNumber - b.blockNumber))
          : filtered.sort((a, b) =>
            -(a.blockNumber - b.blockNumber))

        p('[fetchAccountTxs :: action :: account]', `patching account ${accountId}`)
        let patched = {}
        update(($) => {
          patched = pipe(
            assoc('_status')('idle'),
            assocPath([ 'transactionsByAccount', accountId ])(txs),
            ...Object
              .entries(consensus)
              .map(([ k, v ]) =>
                assocPath([ 'consensus', k ])(v)),
            ...Object
              .entries(patchedAccount)
              .map(([ k, v ]) =>
                assocPath([ 'accountsById', accountId, k ])(v))
          )($)
          return patched
        })

        const { accountsById, _status } = patched
        const accountIds =
          Object.values(accountsById)
            .map(account =>
              [ account.ledger, account.address ].join('::'))

        store.persistAppState({
          accountIds, accountsById, _status, consensus: patched.consensus
        })
          .then(() =>
            p('[fetchAccountsTxs :: action :: account]', 'new app state persisted'))
          .catch(console.error)
      })
  }

  function maybeFetchTransactions (accountId = '', state = {}, newConsensus) {
    const accountIdArr = accountId.split('::')
    const accountLedger = accountIdArr[0]
    const accountAddress = accountIdArr[1]
    const consensus = newConsensus || state.consensus
    const account = state.accountsById[accountId] || {}
    const accountTxs = state.transactionsByAccount[accountId]

    return [[
      accountId,
      accountAddress,
      accountLedger,
      state._status === 'idle',
      account._status !== 'fetching_more',
      account._status !== 'fetching',
      accountTxs == null ||
      account._lastFetch == null ||
      account._lastFetch.blocks[1] < consensus.block
    ]]
      .filter(conditions => conditions.every(Boolean)).map(() => {
        p('[maybeFetchTransactions :: action :: account]',
          'fetch transactions for account', accountId)
        return fetchAccountTxs(
          accountId,
          state,
          consensus,
          { threshold: 1,
            ledgerType:
              (account.type === 'settlement')
                ? 'transferable'
                : 'stromkonto',
            n: fury.createNode()
          })
      })
  }

  return { maybeFetchTransactions, fetchAccountTxs }
}
