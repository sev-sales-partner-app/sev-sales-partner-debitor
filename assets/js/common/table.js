import { b as css, createElement as el, memo, theme } from '../util/view'

const { colors, fontSizes, space } = theme

export const Table = memo(function Table (props) {
  // db dt-l f6 f5-ns collapse mw7 w-100
  // overflow-y-hidden overflow-x-auto nr2 nl2
  const style = css`
    display: block;
    font-size: ${fontSizes[1]};
    margin: 0 -${space[2]} 0 -${space[2]}
    overflow-x: hidden;
    overflow-y: auto;
  `['$notSmall']`
    font-size: ${fontSizes[2]}
    margin: 0
  `['$large']`
    border-collapse: collapse;
    border-spacing: 0;
    display: table;
    max-width: 100%;
    width: 100%;
    table-layout: auto`

  const className = [
    'Table',
    props.className, style['class']
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })

  return el('table', attrs, attrs.children)
})

export const Thead = memo(function Thead (props) {
  const style = css`
    border-bottom: 1px solid ${colors.silver}
    display: block;
  `['$large'](css`
    display: table-row-group;`)

  const attrs = Object.assign({}, props, { className: 'Thead ' + style.class })

  return el('thead', attrs, attrs.children)
})

export const TheadRow = memo(function TheadRow (props) {
  // tr.bb.b--silver.db.dt-row-l.tr
  const style = css`
    display: block;
  `['$large'](css`
    display: table-row`)

  const attrs = Object.assign({}, props, { className: 'Thead-row ' + style.class })

  return el('tr', attrs, attrs.children)
})

export const TheadCell = memo(function TheadCell (props) {
  // 'th.dib.dtc-l.f7.pa2.ttu.tracked.truncate.v-mid.w-100-l'
  const styles = {
    cell: css`
      display: block;
      padding: 0 ${space[2]} ${space[3]} ${space[2]};
      text-align: right;
      vertical-align: bottom;
    `['$large']`
      display: table-cell;
      width: 100%`,

    label: css`
      font-size: ${fontSizes[0]};
      text-transform: uppercase
      text-tracked;
      // text-upper-case;
      text-truncate`, // fixme text-uppercase

    sum: css`
      color: ${props['isnegative'] ? colors.darkRed : 'currentColor'};
      display: inline-block;
      font-family: Courier Next,courier,monospace;
      font-size: ${fontSizes[2]};
      margin-left: ${space[3]};
      text-no-wrap`
  }

  const { colSpan } = props

  return (
    el('th',
      { className: 'Thead-cell ' + styles.cell.class,
        colSpan: colSpan || '4' },
      props.label && el('label', { className: styles.label['class'] }, props.label),
      props.children && el('span', { className: styles.sum['class'] }, props.children)
    )
  )
})

export const Tbody = memo(function Tbody (props) {
  const style = css`
    display: block;
  `['$large']`
    display: table-row-group;`

  const attrs = Object.assign({}, props, { className: 'Tbody ' + style.class })

  return el('tbody', attrs, attrs.children)
})

export const EmptyRow = memo(function EmptyRow (props) {
  const styles = {
    // tr.db.dt-row-l
    row: css`
      display: block
    `['$large']`
      display: table-row`,

    // td.db.dtc-l.pa3.tc
    cell: css`
      display: block;
      padding: ${space[5]};
      text-align: center
    `['$large']`
      display: table-cell`
  }

  return (
    el('tr',
      { className: 'Tbody-row-empty ' + styles.row.class },
      el('td',
        Object.assign({}, props, { className: 'Tbody-cell-empty ' + styles.cell.class }),
        el('samp', {}, props.children)
      )
    )
  )
})

export const TbodyRow = memo(function TbodyRow (props) {
  // tr.db.dt-row-l.hover-bg-near-white'
  // tr.db.dt-row-l.pointer.hover-bg-near-white
  const style = css`
    border-top: 1px solid ${colors.lightGray}
    display: block;
    ${props.onClick ? 'hover-pointer' : ''}
    hover-bg: ${colors.nearWhite}
    `['$large']`
      display: table-row
    `['$firstChild']`
      border-style: none;
      border-width: 0px`

  const className =
    [ 'Tbody-row', props.className, style, props.css && css(props.css) ]
      .filter(Boolean)
      .map(x => typeof x === 'string' ? x : String(x))
      .join(' ')
  const attrs = Object.assign({}, props, { className })

  return el('tr', attrs, attrs.children)
})

export const TbodyCell = memo(function TbodyCell (props) {
  const styles = {

    // td.dib.dtc-l.pa2
    cell: css`
      display: inline-block;
      padding: ${space[2]};
      max-width: 100%;
      vertical-align: bottom
    `['$large']`
      display: table-cell;
    `['$lastChild'](css`
        display: block;
        font-size: ${fontSizes[2]}
      `['$large']`
        display: table-cell;
        width: 100%; // fill`),

    // span.f7
    label: css`
      display: block;
      font-size: ${fontSizes[0]};
      text-no-wrap`,

    // .courier.mt1.nowrap
    value: css`
      color: ${props['isnegative'] ? colors.darkRed : 'currentColor'}
      display: block;
      font-family: Courier Next,courier,monospace;
      margin-top: ${space[1]};
      ${props.align ? 'text-align: ' + props.align : ''};
      text-no-wrap;
      text-truncate`
  }
  const attrs = Object.assign({}, props, { className: styles.value.class })
  attrs.label && delete attrs.label
  attrs['isnegative'] && delete attrs['isnegative']
  attrs.colSpan && delete attrs.colSpan

  const { colSpan } = props

  return el('td',
    { className: 'Tbody-cell ' + styles.cell.class, colSpan },

    props.label &&
      el('label',
        { className: styles.label['class'] },
        props.label),

    props.children && el('span', attrs)
  )
})

export const ShowMoreCell = memo(function ShowMoreCell (props) {
  // 'td.db.dtc-l.f7.tc.pa3'
  const style = css`
    display: block;
    font-size: ${fontSizes[0]};
    padding: ${space[3]}
    text-align: center;
  `['$large'](css`
      display: table-cell`)

  const attrs = Object.assign({}, props)
  attrs['fromblock'] && delete attrs['fromblock']
  attrs['txscount'] && delete attrs['txscount']

  return el('td',
    { className: style.class,
      colSpan: '4' },
    props['fromblock'] === '0'
      ? 'showing all transactions (' + props['txscount'] + ')'
      : el('button', attrs)
  )
})

export const Tfoot = memo(function Tfoot (props) {
  const style = css`
    border-top: 1px solid ${colors.silver};
    display: block;
  `['$large'](css`
      display: table-row-group;`)

  const attrs = Object.assign({}, props, { className: 'Tfoot ' + style.class })

  return el('tfoot', attrs, attrs.children)
})

// todo redundant TheadRow
export const TfootRow = memo(function TfootRow (props) {
  // tr.bb.b--silver.db.dt-row-l.tr
  const style = css`
    display: block;
  `['$large'](css`
      display: table-row`)

  const attrs = Object.assign({}, props, { className: 'Tfoot-row ' + style.class })

  return el('tr', attrs, attrs.children)
})

// todo redundant TheadCell
export const TfootCell = memo(function TfootCell (props) {
  const styles = {
    cell: css`
      display: block;
      padding: ${space[3]} ${space[2]} 0 ${space[2]};
      text-align: right;
      vertical-align: bottom;
    `['$large'](css`
        display: table-cell;
        width: 100%`),

    label: css`
      font-size: ${fontSizes[0]};
      text-transform: uppercase
      text-tracked
      // text-upper-case
      text-truncate`, // fixme text-uppercase

    sum: css`
      color: ${props['isnegative'] ? colors.darkRed : 'currentColor'};
      display: inline-block;
      font-family: Courier Next,courier,monospace;
      font-size: ${fontSizes[2]};
      margin-left: ${space[3]}
      text-no-wrap`
  }

  const { colSpan } = props

  return (
    el('th',
      { className: 'Tfoot-cell ' + styles.cell.class,
        colSpan: colSpan || '4' },
      props.label && el('label', { className: styles.label['class'] }, props.label),
      props.children && el('span', { className: styles.sum['class'] }, props.children)
    )
  )
})
