import { b as css, createElement as el, memo, theme } from '../util/view'
import { Button } from './button'

const { colors, fontSizes, space } = theme

export const InputGroup = memo(function InputGroup (props) {
  const style = css`
    display: block;
    margin-top: ${space[4]}`

  const className =
    [ 'InputGroup', style, props.css && css(props.css) ]
      .filter(x => x != null && x !== '')
      .map(x => typeof x === 'string' ? x : x.toString())
      .join(' ')

  const attrs = Object.assign({}, props, { className })
  attrs.css && delete attrs.css

  return el('label', attrs)
})

export const Label = memo(function Label (props) {
  const styles = {

    base: css`
      display: block;
      font-size: ${fontSizes[1]};
      font-weight: 400;`,

    error: css`color: ${colors.darkRed};`
  }

  const className = [
    'Label',
    props.className,
    styles.base,
    props.error && styles.error,
    props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })
  attrs.css && delete attrs.css
  attrs.as && delete attrs.as

  return el(props.as || 'label', attrs)
})

export const Hint = memo(function Hint (props) {
  const styles = {

    base: css`
      display: block;
      color: ${colors.gray}
      font-size: ${fontSizes[1]};
      font-weight: 400;
       margin-top: ${space[1]};`,

    error: css`color: ${colors.darkRed};`
  }

  const className = [
    'Hint',
    props.className,
    styles.base, // style.hint,
    props.error && styles.error,
    props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })
  attrs.error && delete attrs.error

  return el('div', attrs, props.error ? props.error : attrs.children)
})

export const Input = memo(function Input (props) {
  if (props.type === 'submit') {
    return (
      el(Button,
        Object.assign(
          { as: 'input',
            color: props.color || 'blue' },
          props
        )
      )
    )
  }

  const styles = {
    text: css`
      background-color: ${theme.colors.white};
      border: ${colors.moonGray} solid 1px;
      color: currentColor;
      border-radius: ${theme.radii[2]};
      margin-top: ${space[1]};
      padding: ${space[2]}`,

    file: css`
      border: none;
      padding: ${space[2]} 0;
      margin-top: ${space[1]}`,

    error: css`border-color: ${colors.darkRed};`
  }

  styles.readOnly = styles.file

  const map = { checkbox: 'Checkbox', text: 'Text', submit: 'Submit', file: 'File' }
  const className = [
    map[props.type] || 'Text',
    props.className,
    styles[props.type],
    props.readOnly && styles.readOnly,
    (!styles[props.type] && !props.readOnly) ? styles.text : '',
    props.error && styles.error,
    props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign(
    { id: props.id || props.name || 'no-name',
      name: props.name || props.id || 'no-name',
      type: props.type || 'text' },
    props,
    { className }
  )
  attrs.css && delete attrs.css
  attrs.error && delete attrs.error

  return el('input', attrs)
})

export const Prefix = memo(function Prefix (props) {
  const s = css`
    font-size: ${fontSizes[1]};
    padding-right: ${space[2]}`

  return el('span', Object.assign({ className: 'Prefix ' + s['class'] }, props))
})

export const Suffix = memo(function Suffix (props) {
  const s = css`
    font-size: ${fontSizes[1]};
    padding-left: ${space[2]}`

  const className = [
    'Suffix',
    props.className,
    s,
    props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })

  return el('span', attrs)
})

/*
 Input
 types:
 color
 date
 datetime-local
 email
 month
 number
 range
 search
 tel
 time
 url
 week

 attributes:
 value
 readonly
 disabled
 size
 maxlength

 autocomplete on/off
 autofocus
 form
 formaction
 formenctype
 formmethod
 formnovalidate
 formtarget
 height and width
 list
 min and max
 multiple
 pattern (regexp)
 placeholder
 required
 step
 */
