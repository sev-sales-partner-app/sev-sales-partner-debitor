import { createElement as el, memo } from '../util/view'

export const Spinner = memo(function Spinner (props) {
  // const { fontSizes } = theme

  // const spCircRot = css.$keyframes({
  //   from: 'transform: rotate(0deg);',
  //   to: 'transform: rotate(359deg);'
  // })

  // rely on global css !!!!
  // const styles = {
  //   outer: css`
  //     padding: 10px;
  //     height: 125px;
  //     text-align: center;`,
  //
  //   circle: css`
  //     clear: both;
  //     height: 48px;
  //     width: 48px;
  //     margin: 20px auto;
  //     border: 4px rgba(0, 0, 0, 0.25) solid;
  //     border-top: 4px rgba(0, 0, 0, 1) solid;
  //     border-radius: 50%;
  //     animation: ${spCircRot} .81s infinite linear;`,
  //
  //   tagline: css`
  //     font-size: ${fontSizes[1]};
  //     font-weight: 500;
  //     text-upper-case;
  //     text-tracked`
  // }

  return (
    el('div', { className: 'g-Spinner' }, // ${styles.outer.class}

      el('div', { className: 'circle' }), // ${styles.circle.class}

      props.children &&
        el('h5', { className: 'tagline' }, // ${styles.tagline.class}
          props.children
        )
    )
  )
})
