import { b as css, createElement as el, memo, theme } from '../util/view'

const { colors, fontSizes, space, radii } = theme

export const Button = memo(function Button (props) {
  const styles = {

    base: css`
      background-color: ${colors.transparent};
      border: 1px solid ${colors.midGray};
      border-radius: ${radii[2]};
      color: ${colors.gray};
      font-weight: bold;
      padding: ${space[2]} ${space[3]};
      text-decoration: none;
      hover-dim`,

    blue: css`
      border: 1px solid ${colors.blue};
      color: ${colors.blue};`,

    danger: css`
      border: 1px solid ${colors.darkRed};
      color: ${colors.darkRed};`,

    disabled: css`
      background-color: ${colors.transparent};
      border: 1px solid ${colors.midGray};
      border-radius: ${radii[2]};
      color: ${colors.gray};
      font-weight: bold;
      padding: ${space[2]} ${space[3]};
      text-decoration: none;
      opacity: 0.4;`,

    large: css`
      border-radius: ${radii[3]};
      font-size: ${fontSizes[2]};
      padding: ${space[3]}`
  }

  const className = [
    'Button',
    props.className,
    props.disabled ? styles.disabled : styles.base,
    styles[props.color],
    styles[props.size],
    props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })

  attrs.as && delete attrs.as
  attrs.css && delete attrs.css

  return el(props.as || 'button', attrs)
})
