import { b as css, createElement as el, memo, theme } from '../util/view'

const { fontSizes } = theme

export const Title = memo(function Title (props) {
  const style = css`
    font-size: ${fontSizes[1]};
    text-transform: uppercase;
    margin: 1.5rem 0;
    text-truncate;
    text-tracked`

  const className = [
    'Title', props.className, style, props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })
  attrs.css && delete attrs.css
  attrs.as && delete attrs.as

  return el(props.as || 'h1', attrs, el('span', {}, attrs.children))
})
