import { b as css, createElement as el, memo, theme } from '../util/view'

const { colors, space } = theme

export const Option = memo(function Option (props) {
  return el('option', props)
})

export const Select = memo(function Select (props) {
  const styles = {
    base: css`
      border-color: ${colors.moonGray};
      margin-top: ${space[1]};`,

    error: css`border-color: ${colors.darkRed};`
  }

  const className = [
    'Select', styles.base['class'], props.error && styles.error['class']
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign(
    { id: props.id || props.name || 'no-name',
      name: props.name || props.id || 'no-name',
      type: props.type || 'text' },
    props,
    { className })

  return el('select', attrs)
})
