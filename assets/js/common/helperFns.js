import { defaultTo } from '../util/fp'

// calcBalance :: Object -> Object
export function calcBalance ({ credit, debit }) {
  return [[ debit != null, credit != null ]]
    .filter(conditions => conditions.every(Boolean))
    .map(() => {
      return {
        base: defaultTo(0)(credit.base - debit.base),
        value: defaultTo(0)(credit.value - debit.value)
      }
    })[0] || { base: 0, value: 0 }
}

// calcVBalance :: Object -> Number
export function calcVBalance (address = '', transactions = []) {
  const isPayableForAddress = isPayableFor(address)
  return [[
    Array.isArray(transactions),
    typeof address === 'string'
  ]].filter(c => c.every(Boolean))
    .map(() =>
      transactions.reduce((acc, tx) => {
        isPayableForAddress(tx)
          ? acc -= Number.parseInt(tx.value, 16)
          : acc += Number.parseInt(tx.value, 16)
        return acc
      }, 0)
    )[0] || 0
}

// fromOrSender :: Object -> String
export function fromOrSender (tx = {}) {
  return tx['from'] ? 'from' : 'sender'
}

// toOrRecipient :: Object -> String
export function toOrRecipient (tx = {}) {
  return tx.to ? 'to' : 'recipient'
}

// isInvoic :: String -> Boolean
export function isInvoic (data) { return Number.parseInt(data, 16) === 0 }

// isPayableFor :: String -> Object -> Boolean
export function isPayableFor (accountAddress = '') {
  return function (tx = {}) {
    const fromOrSenderTx = fromOrSender(tx)
    if (tx[fromOrSenderTx] == null) return false

    const assertion = tx[ fromOrSenderTx ].toLowerCase() === accountAddress.toLowerCase()

    if ('data' in tx) return !isInvoic(tx.data) ? assertion : !assertion
    else return assertion
  }
}
