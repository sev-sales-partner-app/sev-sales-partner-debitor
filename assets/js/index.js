// import patchConsole from '../util/debug'
import { listenToRouteChanges } from './util/router'
import { stream, scan } from './util/streamy'
import { createElement as create, render } from './util/view'

import { app, App } from './app'

const T = (x, f) => f(x)
const update = stream()

Promise.resolve()
  .then(app.initialState)
  .then(initialState => {
    const actions = app.actions(update)
    const states = scan(T, initialState, update)
    const computedStates = states.map(app.computeState)

    computedStates.map(state =>
      app.services.map(service =>
        service(state, update, actions)
      )
    )

    render(create(App, { states: computedStates, actions }), document.getElementById('app'))

    listenToRouteChanges(update)
    // patchConsole() // todo turn on later

    if (process.env.NODE_ENV !== 'production') {
      require('meiosis-tracer')({
        streams: [
          { stream: states, label: 'states (initial)' },
          { stream: computedStates, label: 'computed states' }
        ]
      })
    }
  })
  .catch(err => console.error(err))
