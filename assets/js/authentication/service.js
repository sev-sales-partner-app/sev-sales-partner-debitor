import { p } from '../util/debug'

import localStore from '../data/localStore'
import furyRpc from '../data/furyRpc'
import serverApi from '../data/serverApi'

import { validateToken, validateTokenPayload } from '../validation/credentials'

import { SettlementsPage, SignInPage, UserAccountPage } from '../util/router'
import { settlementLedger, vat } from '../util/constants'
import { assoc, assocPath, dissocPath, pipe } from '../util/fp'

import * as patch from './patches'

function validateJWT (token) {
  const validatedToken = validateToken(token)
  const validatedPayload = validateTokenPayload(token)
  const errors = [ validatedToken, validatedPayload ]
    .filter(({ error }) => error)
    .map(({ error }) => error)

  return { validatedToken, validatedPayload, errors }
}

// next action factory

// Service :: (fury, store, server) -> (state, update) -> void 0
export const Service = (fury = furyRpc, store = localStore, server = serverApi) => {
  // generateUserNode :: (Object, Object) -> Object
  function generateUserNode (tokenPayload, credentials) {
    console.assert(tokenPayload.extid.value === credentials.username.value,
      '[generateUserNode :: nextAction]',
      'assert validatedPayload.value.extid === credentials.username.value')

    const extid = credentials.username.value
    const user = {
      extid,
      privateKey: // FIXME unsecure !!!
        fury.utils.decrypt(tokenPayload.key.value, credentials.passphrase.value)
    }
    function Node () { return furyRpc.createNode(user) }

    return { user, Node }
  }

  // fetchAccounts :: (user :: Object, node :: Object, token :: String) -> Promise<any>
  function fetchAccounts (user, node, token) {
    const { extid } = user
    const { address } = node.wallet

    return Promise.all([
      fury.fetchLedger(node, false), // debtor ledger
      server.getCreditors(token)
        .then(res => res.data, err => {
          server.logError(err)
          return []
        })
    ])
      .then(([ debtorLedger, creditors ]) => {
        const userAccount = {
          address: address.toLowerCase(),
          ledger: debtorLedger.toLowerCase(),
          extid,
          label: 'SEV Provider account (user)',
          ref: UserAccountPage,
          type: 'user',
          credit: { value: 0, base: 0 },
          debit: { value: 0, base: 0 },
          _lastFetch: { blocks: [0, 0], time: 0 },
          _status: 'idle'
        }
        const settlementAccount = {
          address: address.toLowerCase(),
          ledger: settlementLedger.toLowerCase(),
          extid: extid + '::settlements',
          label: 'SEV Settlement Account',
          ref: SettlementsPage,
          type: 'settlement',
          credit: { value: 0, base: 0 },
          debit: { value: 0, base: 0 },
          _lastFetch: { blocks: [0, 0], time: 0 },
          _status: 'idle'
        }

        const vatAccount = {
          address: vat.toLowerCase(),
          ledger: debtorLedger.toLowerCase(),
          extid: extid + '::vat',
          label: 'SEV Umsatzsteuer Account',
          // ref: SettlementsPage,
          type: 'ust',
          credit: { value: 0, base: 0 },
          debit: { value: 0, base: 0 },
          _lastFetch: { blocks: [0, 0], time: 0 },
          _status: 'idle'
        }

        const creditorAccountsList = creditors.map(({ address, extid }) => {
          return {
            address: address.toLowerCase(),
            ledger: debtorLedger.toLowerCase(),
            extid,
            ref: '', // AccountPage
            type: extid.split('::').length > 1
              ? 'creditor-entitlement'
              : 'creditor',
            credit: { value: 0, base: 0 },
            debit: { value: 0, base: 0 },
            _lastFetch: { blocks: [0, 0], time: 0 },
            _status: 'idle'
          }
        })

        const accountsById =
          [ userAccount,
            settlementAccount,
            vatAccount,
            ...creditorAccountsList ]
            .reduce((acc, account) => {
              acc[ [ account.ledger, account.address ].join('::') ] = account
              return acc
            }, {})
        const accountIds = Object.keys(accountsById)

        return { accountIds, accountsById }
      })
  }

  // authenticate :: (user :: Object) -> ({ with ::(a, b, c) -> Promise<any> })
  function authenticate (user = {}) {
    return {
      with: function (credentials = {}, token = '', node = {}) {
        return server.checkAuth(token)
          .then(() =>
            fetchAccounts(user, node, token),
          err => {
            server.logError(err)
            throw err
          })
          .then(({ accountIds, accountsById }) => ({
            accountIds,
            accountsById,
            auth: {
              authenticated: true,
              message: '',
              status: 'idle',
              token,
              username: credentials.username
            },
            user
          }))
      }
    }
  }

  function persistAuthenticated ({ accountIds, accountsById, auth, consensus, user }) {
    return server.perpetuateAuth(auth.token)
      .then(res => {
        const permToken = validateToken(res.data)
        auth.token = permToken.error ? auth.token : permToken.value
        store.persistAppState({ consensus, auth, accountIds, accountsById, user })
          .catch(err => console.error('[persistAuthenticated :: service :: authentication]', err))

        return { accountIds, accountsById, auth, user }
      })
  }

  //  authenticator :: (states, updates) -> void 0
  let prevStatus = 'idle'
  function authenticator (state, update) {
    if (state.auth.status === prevStatus) return
    if (state.pageId !== SignInPage || !state.params.token) return

    const { auth, params } = state
    const { authenticated, username } = auth
    const { token } = params
    p('[authenticator :: service :: authentication]', 'is firing')

    if (authenticated) {
      p('[authenticator :: service :: authentication]', 'token found')
      prevStatus = 'idle'
      update(pipe(
        dissocPath(['params', 'token']),
        dissocPath(['params', 'authredirect']),
        patch.authStatus('idle'),
        patch.authMessage(''),
        patch.redirectToUrl(params.authredirect)))
      return
    }

    const {
      validatedToken,
      validatedPayload,
      errors
    } = validateJWT(token)

    if (errors.length !== 0) {
      let msg = 'Authentication failed. Token not valid. Please try again or contact someone.'
      console.error('[authenticator :: service :: authentication]', errors)
      prevStatus = 'idle'
      update(patch.authFail(msg, new Error(errors[0])))
      return
    }

    const tokenPayload = validatedPayload.value

    p('[authenticator :: service :: authentication]',
      'Token payload ok:',
      Math.round(((new Date().getTime() / 1000) - tokenPayload.iat.value) / 120) + ' min since auth'
    )

    const creds = { // todo not secure, would be better to habe a proper passphrase
      username: tokenPayload.extid,
      passphrase: {
        value: tokenPayload.scope.value +
            '::' + (username.value || tokenPayload.extid.value),
        error: ''
      }
    }

    // p('[authenticator :: service :: authentication]',
    //   'validated credentials: ', creds)

    const { user, Node } = generateUserNode(tokenPayload, creds)
    const node = Node()

    if (node.wallet.address !== tokenPayload.address.value) {
      prevStatus = 'idle'
      update(patch.authFail(
        'Authentication failed.',
        new Error('[authenticator :: service :: authentication]' + ' Wallet keys don\'t match')))
    }

    authenticate(user).with(creds, validatedToken.value, node)
      .then(res => persistAuthenticated(Object.assign({}, res, { consensus: state.consensus })))
      .then((patchValues) => {
        p('[authenticator :: service :: authentication]', 'updating state with:', patchValues)
        const patchAuthentication = pipe(
          dissocPath([ 'params', 'authredirect' ]),
          dissocPath([ 'params', 'token' ]),
          assoc('accountIds')(patchValues.accountIds),
          assoc('accountsById')(patchValues.accountsById),
          assoc([ 'user' ])(patchValues.user),
          assocPath([ 'auth', 'authenticated' ])(true),
          assocPath([ 'auth', 'token' ])(patchValues.auth.token),
          patch.authStatus(patchValues.auth.status),
          patch.authMessage(patchValues.auth.message),
          patch.authUsername(patchValues.auth.username),
          patch.redirectToUrl(params.authredirect)
        )

        prevStatus = patchValues.auth.status
        update(($) => patchAuthentication($))
      })
      .catch(err => {
        console.error('[authenticator :: service :: authentication]', 'Authentication failed', err)
        prevStatus = 'idle'
        update(patch.authFail('Authentication failed', err))
      })
  }
  return authenticator
}
