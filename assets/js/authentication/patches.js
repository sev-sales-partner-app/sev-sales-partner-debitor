import { assoc, assocPath, dissocPath, pipe } from '../util/fp'
import {
  defaultRoute,
  prefix,
  navigateTo,
  parseUrl,
  SignInPage
} from '../util/router'

// authMessage :: a :: String -> b :: Object -> c :: Object
export const authMessage = assocPath([ 'auth', 'message' ])

// authStatus :: a :: String -> Object -> Object
export const authStatus = assocPath([ 'auth', 'status' ])

export function authUsername (username = {}) {
  return pipe(
    assocPath(['auth', 'username', 'value'])(username.value),
    assocPath(['auth', 'username', 'error'])(username.error || '')
  )
}

// navigateToPage :: (String, Object?, url?) -> a :: Object -> b :: Object
function navigateToPage (pageId, params = {}, url) {
  return pipe(
    assoc('pageId')(pageId),
    assoc('url')(url || navigateTo(pageId, params)['url']),
    ...Object
      .entries(params)
      .map(([k, v]) =>
        assocPath(['params', k])(v))
  )
}

// navigateToUrl :: String -> a :: Object -> b :: Object
export function redirectToUrl (authredirect) {
  const route = authredirect ? decodeURIComponent(authredirect).split(prefix)[1] : defaultRoute
  const { pageId, params, url } = parseUrl(prefix + route)
  return navigateToPage(pageId, params, url)
}

// authFail :: (String, Error?) -> a :: Object -> b :: Object
export const authFail = (message = 'Auth failed', err) => {
  err && console.error('[authFail]', err)

  return pipe(
    assocPath(['auth', 'authenticated'])(false),
    assocPath(['auth', 'token'])(''),
    assocPath(['auth', 'username', 'value'])(''),
    authMessage(message),
    authStatus('idle'),
    dissocPath(['params', 'token']),
    navigateToPage(SignInPage) // or assoc('pageId')(SignInPage),
  )
}
