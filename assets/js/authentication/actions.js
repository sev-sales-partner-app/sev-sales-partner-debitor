import { p } from '../util/debug'
import { pipe } from '../util/fp'
import localstore from '../data/localStore'
import serverapi from '../data/serverApi'
import * as patch from './patches'

export const actions = (update, store = localstore, server = serverapi) => {
  function signOutUser () {
    p('[signOutUser :: action]', 'sign out user')
    update(patch.authFail('You signed out.'))

    store
      .clearAppState()
      .catch(err => console.error(err))
  }

  function submitSignInRequest ({
    validatedCreds = {},
    callbackUrl = '',
    role = 'debtor',
    errors = []
  }) {
    if (errors.length) return

    p('[submitSignInRequest :: action :: authentication]',
      'validated credentials', validatedCreds)
    update(pipe(
      patch.authStatus('in_progress'),
      patch.authMessage('Sending login mail...')))

    const { username } = validatedCreds
    server
      .submitCredentials({
        username: username.value,
        callbackUrl,
        role
      })
      .then(res => {
        p('[submitSignInRequest :: action :: authentication]',
          'set state.auth.authStatus to "e_mail_sent"')
        update(pipe(
          patch.authMessage(res.data.toString()),
          patch.authStatus('e_mail_sent')
        ))
      }, err => {
        server.logError(err)
        throw err
      })
      .then(() => {
        p('[submitSignInRequest :: action :: authentication]',
          'persist auth in local storage')
        store
          .persistAppState({ auth: validatedCreds })
          .catch(err => console.error(err))
      })
      .catch(err => {
        let msg
        if (err.response) {
          msg = err.response.data.message
        } else if (err.request) {
          msg = 'Server did not respond.'
        } else {
          msg = err.message
        }
        update(
          patch.authFail('Authentication failed :(   ' + msg, err))
      })
  }

  return { signOutUser, submitSignInRequest }
}
