import { p } from '../util/debug'
import { assoc, assocPath, dissoc, pipe, tap } from '../util/fp'
import { getUrl, SignInPage } from '../util/router'

// authIntercept :: Compute -> a :: Object -> b :: Object
export function authIntercept (state) {
  const { auth, params, pageId } = state

  // (I) assertions
  const isAuthenticated = auth.authenticated === true
  const tokenFound = 'token' in params || (!isAuthenticated && auth.token)
  const isSignInPage = pageId === SignInPage

  // (II) construct authredirect param
  const cleanParams = pipe(dissoc('token'), dissoc('authredirect'))
  const url = getUrl(pageId, cleanParams(params))
  const authredirect =
    encodeURIComponent(window.location.origin + '/' + url)

  // (III) patches
  const logOk = tap(() =>
    p('[authIntercept :: compute :: authentication]',
      'All good, you are signed in')
  )
  const logToken = tap(() =>
    p('[authIntercept :: compute :: authentication]', 'found token')
  )
  const logIntercept = tap(() =>
    p('[authIntercept :: compute :: authentication]',
      'Not authenticated.',
      `redirecting from ${window.location.href} to ${getUrl(SignInPage, { authredirect })}...`)
  )

  const redirectToSignIn = pipe(
    assoc('pageId')(SignInPage),
    assoc('params')({ authredirect }),
    assoc('url')(getUrl(SignInPage, { authredirect })),
    logIntercept)

  const stayOnSignInPage =
    !isAuthenticated && isSignInPage
      ? x => x
      : redirectToSignIn

  const proccessToken = pipe(
    assocPath(['auth', 'status'])('in_progress'),
    stayOnSignInPage,
    logToken
  )

  if (isAuthenticated) return logOk
  else if (tokenFound) return proccessToken
  else if (!isSignInPage) return redirectToSignIn
  else return stayOnSignInPage
}
