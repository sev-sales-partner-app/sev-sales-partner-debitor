import { getUrl, HomePage } from '../util/router'
import { createElement as el, Fragment } from '../util/view'
import { Spinner, Notification, Title, Button } from '../common'
import { PageLayout } from '../layout'
import { credentials, Credentials } from '../credentials'
import { actions } from './actions'
import { authIntercept } from './compute'
import { Service } from './service'

export const authentication = {

  initialState: () =>
    Object.assign({
      authenticated: false,
      message: '',
      status: 'idle',
      token: ''
    }, credentials.initialState()),

  actions: (update) =>
    Object.assign(
      credentials.actions(update),
      actions(update)),

  computeState: authIntercept,
  service: Service()
}

export function Authentication ({ actions, id = 'auth', state }) {
  const { authenticated, message, status, username } = state[ id ]

  if (status === 'in_progress') return el(Spinner, null, message || 'Authenticating...')

  if (status === 'e_mail_sent') {
    return (
      el(PageLayout,
        { size: 'login' },

        el(Notification,
          { size: 'fullscreen' },

          el(Title, null, 'You have mail'),

          el('p', null, message || 'A log-in link was sent to your e-mail address.'),

          el('p', null,
            el('span', null, 'To sign into your account. Please click on the link in the email.'),
            el('br'),
            el('span', null, 'You can close this window.')
          )
        )
      )
    )
  }

  return (
    el(PageLayout,
      { size: 'login' },

      el(Title, null, !authenticated ? 'Please sign in' : `Hello ${username.value},`),

      message && el('p', null, message),

      !authenticated
        ? el(Credentials, { actions, id, state })
        : el(Fragment, null,

          el('p', null, 'You are authenticated!'),

          el(Button,
            { onClick: actions.signOutUser },
            'Sign out'),

          el('a',
            { href: getUrl(HomePage) },
            'Back to home page')
        )
    )
  )
}
