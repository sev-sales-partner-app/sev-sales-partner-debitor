import { syncLocationBar } from '../util/router'
import { Component, createElement as el } from '../util/view'

import { Root } from '../root'

export class App extends Component {
  constructor (props) {
    super(props)
    this.state = props.states()
    this.skippedFirst = false
    this.services = props.services
  }

  componentDidMount () {
    const setState = this.setState.bind(this)
    this.props.states.map(state => {
      this.skippedFirst
        ? setState(state)
        : this.skippedFirst = true
    })
  }

  render () {
    const state = this.state
    const { actions } = this.props

    syncLocationBar(state)

    return el(Root, { actions, state })
  }
}
