import { mergeDeepRight } from '../util/fp'
import { parseUrl } from '../util/router'

import furyRpc from '../data/furyRpc'
import localStore from '../data/localStore'

import { authentication } from '../authentication'
import { creditors } from '../creditors'
import { account } from '../account'
import { settlement } from '../settlement'

const initialState = () => {
  const { pageId, params, url } = parseUrl()
  return Promise.all([
    localStore.fetchPersistedState(),
    furyRpc
      .fetchCurrentBlockNumber()
      .then(block => localStore.matchBlockTime(block, furyRpc))
      .catch(err => {
        console.error(err)
        return void 0
      })
  ])
    .then(([ state, consensus ]) =>
      // note state could be null & consensus could be void 0
      Object.assign({}, state, consensus && { consensus }))
    .then(mergeDeepRight({
      consensus: { block: 0, time: 0 },
      pageId,
      params,
      url,
      auth: authentication.initialState(),
      user: {},
      accountIds: [],
      accountsById: {},
      transactionsByAccount: {},
      _bxsLastFetch: 0,
      _status: 'idle'
    }))
}

export const app = {
  initialState,

  actions: (update) =>
    Object.assign(
      {},
      authentication.actions(update),
      creditors.actions(update),
      account.actions(update),
      settlement.actions(update)),

  computeState: state => [
    authentication.computeState,
    settlement.computeState
  ].reduce((x, f) => f(x)(x), state), // Patchinko version: .reduce((x, f) => P(x, f(x)), model)

  services: [
    authentication.service,
    creditors.service,
    account.service,
    settlement.service
    // root.service
  ]
}

export { App } from './view'
