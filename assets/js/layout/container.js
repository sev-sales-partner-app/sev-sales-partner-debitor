import { b as css, createElement as el, memo } from '../util/view'

export const Container = memo(function Container (props) {
  const styles = {
    base: css`
     width: 100%;
     margin-left: auto;
     margin-right: auto`,

    // TODO add theme widths
    login: css`max-width: 34rem`, // measure-wide
    narrow: css('max-width: 63rem;'),
    wide: css('max-width: 96rem;')
  }

  const className = [
    'Layout-container' + (props.size ? `-${props.size}` : ''),
    styles.base,
    props.size && styles[props.size],
    props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })
  attrs.css && delete attrs.css

  return el('div', attrs)
})
