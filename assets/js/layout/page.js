import { createElement as el, memo } from '../util/view'
import { Container } from './container'
import { Rim } from './rim'

export const PageLayout = memo(function PageLayout (props) {
  return (
    el(Rim, { },
      el(Container, { size: props.size || 'narrow' }, props.children)
    )
  )
})
