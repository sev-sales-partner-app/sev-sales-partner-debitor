import { b as css, createElement as el, memo } from '../util/view'

export const Main = memo(function Main (props) {
  const style = css`flex: 1 0 auto;`

  return (
    el('main',
      { className: [ 'Layout-main', style['class'] ].join(' ') },
      props.children
    )
  )
})
