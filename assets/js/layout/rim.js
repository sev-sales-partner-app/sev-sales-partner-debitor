import { b as css, createElement as el, memo, theme } from '../util/view'

const { space } = theme

export const Rim = memo(function Rim (props) {
  const style = css`
    padding: ${space[3]}
  `.$notSmall(css`
    padding: ${space[3]} ${space[5]};`)

  const className = [
    'Layout-rim', props.className, style, props.css && css(props.css)
  ].filter(x => x != null && x !== '')
    .map(x => typeof x === 'string' ? x : x.toString())
    .join(' ')

  const attrs = Object.assign({}, props, { className })

  attrs.as && delete attrs.as
  attrs.css && delete attrs.css

  return (
    el(props.as || 'div', attrs)
  )
})
