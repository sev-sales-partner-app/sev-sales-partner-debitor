// import { p } from '../util/debug'
import { blankHref } from '../util/constants'
import {
  HomePage,
  // SignInPage,
  // CreditorsPage,
  EntitlementsPage,
  SettlementsPage,
  // UserAccount,
  // EntitlementAccount,
  // IncomeAccount,
  getUrl
} from '../util/router'
import { b as css, createElement as el, memo, theme } from '../util/view'
import { Container } from './container'
import { Rim } from './rim'

const navItems = [
  { id: HomePage, name: 'Home' },
  // { id: SignInPage, name: 'Log in' },
  { id: EntitlementsPage, name: 'Entitlements' },
  { id: SettlementsPage, name: 'Abrechnungen', filter: 'received' } // ,
  // { id: UserAccount, name: 'Account' },
  // { id: EntitlementAccount, name: 'Provisionsanspruch' },
  // { id: IncomeAccount, name: 'Einnahmen' }
]

const { colors, fontSizes, space } = theme

export const Header = memo(function Header ({ state }) {
  return el(Rim, { as: 'header' },
    el(Container,
      { size: 'wide',
        css: css`
            display: flex;
            flex-direction: column;
            width: 100%;
            z-index: 1;
          `['$media']('(min-width: 600px)', css`
              flex-align-items: baseline;
              flex-direction: row;
              flex-wrap: wrap;`)
      },

      // Header Brand
      el('div',
        { className: ['Header-brand', css`margin-right: auto;`].join(' ') },

        el('a',
          {
            title: 'Home',
            href: getUrl(HomePage),
            className: css`
                color: ${colors.black70};
                display: block;
                font-size: ${fontSizes[2]};
                font-weight: 500;
                min-height: 56px;
                padding: 16px 0;
              `['$notSmall'](css`
                  font-size: ${fontSizes[3]};`)
              .link
              .class
          },
          'SEV',

          el('small',
            {
              className: css`
                  font-weight: 200;
                  white-space: nowrap;`['class']
            },
            ' Debtor ' + state.pageId
          )
        )
      ),

      // Header Nav
      el('nav',
        {
          className: 'Header-nav ' + css`
            display: flex;
            flex-direction: column;
            margin-top: ${space[0]};
          `['$media']('(min-width: 600px)', css`
              flex-direction: row;
              flex-wrap: wrap;`)
            .class
        },

        navItems.map(item =>
          el('a',
            { key: item.id,
              href: item.id
                ? getUrl(item.id, item.filter ? { filter: item.filter } : {})
                : blankHref,
              className: 'Header-nav-item ' + css`
                font-size: ${fontSizes[1]};
                font-weight: 600;
                padding: ${space[2]} 0;
                margin-top: ${space[1]};
                color: ${state.pageId === item.id ? colors.blue : colors.black70};
                hover-color: ${colors.blue}
              `['$firstChild'](css`
                  margin-left: 0
              `)['$lastChild'](css`
                  margin-right: 0;
              `)['$media']('(min-width: 600px)', css`
                  padding: ${space[3]} 0;
                  margin: 0 ${space[3]};`)
                .link
                .class },
            item.name
          )
        )
      )
    )
  )
}, (prev, next) => prev.state['pageId'] === next.state['pageId'])
