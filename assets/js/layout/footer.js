import { b as css, createElement as el, theme } from '../util/view'
import { Container } from './container'
import { Rim } from './rim'

const { colors, fontSizes, space } = theme

export function Footer ({ actions, state }) {
  const sticky = css`flex-shrink: 0`

  const style = css`
    border-color: ${colors.black10};
    color: ${colors.black60};
    display: flex;
    font-size: ${fontSizes[0]};
    flex-direction: column;
    line-height: 1.5em;
    padding: ${space[3]} ${space[0]}
  `['$large'](css`
    flex-direction: row;
    justify-content: space-between;
`)

  const { block, time } = state.consensus || {}

  return (
    el(Rim,
      { as: 'footer',
        css: sticky },

      el(Container,
        { css: style,
          size: 'wide' },

        el('samp', null,
          `#Block ${block} @ ${new Date(time * 1000).toLocaleString('de-De')}`
        ),

        state.auth.authenticated &&
          el('samp', null,
            state.user.extid + ' | ',
            el('span',
              {
                className: css`
                  hover-color: ${colors.blue}
                  // hover-pointer: void;
                  hover-underline: no-op;
                `['class'],

                onClick: actions.signOutUser
              },
              'Sign out'
            )
          )
      )
    )
  )
}
