export const blankHref = 'javascript://'

if (
  !process.env.SETTLEMENT_LEDGER ||
  !process.env.VAT_LEDGER ||
  !process.env.REMOTE_API_HOST ||
  !process.env.LOCAL_API_HOST ||
  !process.env.RPC_HOST
) {
  let msg =
    'Environment variables not set. Provide variables in .env file prior to build (see ".env.example").'
  window.alert(msg)
  throw new Error(msg)
}

// addresses
export const OxOO = '0x0000000000000000000000000000000000000000'
export const settlementLedger = process.env.SETTLEMENT_LEDGER
export const vat = process.env.VAT_LEDGER

// Hosts
export const apiHost =
  process.env.NODE_ENV === 'production'
    ? process.env.REMOTE_API_HOST
    : process.env.LOCAL_API_HOST
export const rpcHost = process.env.RPC_HOST // te
export const abiLocation = '/vendor/smart_contractsV0554'
