// https://gitter.im/foxdonut/meiosis?at=5c76ff43e1446a6ebe575c19
// https://github.com/foxdonut/meiosis/blob/v1.4.1/tutorial/lessons/06-scan-mithril.md

export function stream (init) {
  let current = init
  const mapFns = []

  function Stream (val) {
    if (val == null) return current
    current = val
    // mapFns.forEach(fn => fn(val))
    for (let i in mapFns) mapFns[i](val)
  }

  Stream.map = function (mapFn) {
    // const newInit = (current == null) ? undefined : mapFn(current)
    const newStream = stream((current == null) ? undefined : mapFn(current))
    mapFns.push(val => newStream(mapFn(val)))

    return newStream
  }

  return Stream
}

export function scan (fn, acc, origin) {
  const newStream = stream(acc)
  let accumulated = acc

  origin.map(val => newStream((accumulated = fn(accumulated, val))))

  return newStream
}
