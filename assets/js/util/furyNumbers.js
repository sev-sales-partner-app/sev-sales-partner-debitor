import * as d3 from 'd3-format'

d3.formatDefaultLocale({
  'decimal': ',',
  'thousands': '.',
  'grouping': [3],
  'currency': ['', '\u00a0€'],
  'percent': '\u202f%'
})

export const $multiplier = 10000000
const $defaultMultiplier = 10000000

export function furyTo$ (value, divisor = $defaultMultiplier) {
  return (Math.round((value / divisor) * 100) / 100)
}

furyTo$.string = function (value, divisor) {
  return d3.format('$,.2f')(furyTo$(value, divisor))
}

export function $toFury (value, multiplier = $defaultMultiplier) {
  if (Number.isNaN(Number(value)) || value == null) {
    return NaN
  }
  return Math.round(Number(value) * multiplier)
}

export function furyTokWh (base, divisor = 1000) {
  return (Math.round((base / divisor) * 100) / 100)
}

furyTokWh.string = function (base, divisor) {
  const isFloat = n => Number(n) === n && n % 1 !== 0
  const v = furyTokWh(base, divisor)
  const f = isFloat(v) ? d3.format(',.2f') : d3.format(',.0f')
  return [f(v)].concat(['\u00a0kWh']).join('')
}
