// credit
// https://github.com/porsager/wright/blob/master/lib/log.js
// https://github.com/porsager/flems/blob/master/src/srcdoc/index.js

const log = window.console.log

function patch ({ original, monkey, returnFirst, debugging }) {
  return function (first) {
    if (debugging) {
      addTime(arguments);
      (original || log).apply(console, arguments)
      return returnFirst && first
    }
  }
}

function addTime (args) {
  const now = new Date()
  const h = pad(now.getHours(), 2)
  const m = pad(now.getMinutes(), 2)
  const s = pad(now.getSeconds(), 2)
  const ms = pad(now.getMilliseconds(), 3)
  const stamp = '\x1b[2m' + h + ':' + m + ':' + s + '.' + ms + '\x1b[0m'
  Array.prototype.unshift.call(args, stamp)
}

function pad (str, len) {
  str = String(str)
  while (str.length < len) { str = '0' + str }

  return str
}

export const p = patch({
  original: null,
  monkey: 'print',
  returnFirst: true,
  debugging: process.env.NODE_ENV !== 'production'
})

export default function patchConsole (debugging = process.env.NODE_ENV !== 'production') {
  const monkeys = ['log', 'error', 'trace', 'warn', 'info', 'time', 'timeEnd']
  if (debugging === true) {
    monkeys.forEach(monkey => {
      const original = window.console[monkey]
      window.console[monkey] = patch({ original, monkey, debugging })
    })
  }
}

// log.error = function (err) {
//   if (!err) return
//   Array.prototype.unshift.call(arguments, '\x1b[31mError:\x1b[0m')
//   addTime(arguments)
//   console.error.apply(console, arguments)
// }
//
// log.debug = function (a, b, c) {
//   if ((a || b || c) && log.debugging) {
//     Array.prototype.unshift.call(arguments, '\x1b[32mDebug:\x1b[0m')
//     log.apply(null, arguments)
//   }
// }

// export function log () {
//   addTime(arguments)
//   console.log.apply(console, arguments)
// }

// export function p (first) {
//   if (log.debugging) {
//     log.apply(console, arguments)
//     return first
//   }
// }
