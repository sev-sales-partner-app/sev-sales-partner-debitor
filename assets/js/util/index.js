export const now = () => Math.round(new Date().getTime() / 1000)

// VAT
export const calcVatValue = (net, rate = 0.19) => net * rate
export const netToGross = (net, rate = 0.19) => net + calcVatValue(net, rate)
export const grossToNet = (gross, rate = 0.19) => gross / (rate + 1)

// // Using reduce, courtesy Barney Carroll (https://github.com/barneycarroll)
// export const get = (object, path) =>
//   path.reduce((obj, key) => obj == null ? undefined : obj[key], object)

// export const onChange = (stream, path, handler) => {
//   let previous = null
//   stream.map(state => {
//     const value = get(state, path)
//     if (value != null && value !== previous) {
//       previous = value
//       handler(state)
//     }
//   })
// }
