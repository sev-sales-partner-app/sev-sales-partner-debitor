import Mapper from 'url-mapper'

// pageIds
// export const EntitlementAccount = 'p-entitlement'
// export const SettlementAccount = 'p-settlements'
// export const DebitorAccount = 'p-debitor'
// export const QueriedAccount = 'p-queried'
// export const IncomeAccount = 'p-income'

export const HomePage = 'p-home'
export const SignInPage = 'p-signIn'

export const CreditorsPage = 'p-creditors'
export const EntitlementsPage = 'p-entitlement-accounts'

export const AccountPage = 'p-account'
export const UserAccountPage = 'p-user-account'
export const SettlementsPage = 'p-settlement-account'

export const SettlementPage = 'p-settlement'

// CreditorsPage -> p-creditors (list creditors accounts)
// EntitlementsPage -> p-entitlements (list creditors entitlements accounts)

// AccountPage  -> p-account (list account txs)
// SettlementsPage -> p-settlements (list account txs)

// SettlementPage -> p-settlement (show single tx)
// TransactionPage -> p-transaction (show single tx)

export const prefix = '#'
export const defaultRoute = '/'

const routeMappings = {
  '/': () => ({ pageId: UserAccountPage }),
  '/signin': () => ({ pageId: SignInPage }),
  '/creditors': () => ({ pageId: CreditorsPage }),
  '/creditors/entitlements': () => ({ pageId: EntitlementsPage }),
  '/account/:ledger/:address/:filter?': () => ({ pageId: AccountPage }),
  // '/user/:filter?': () => ({ pageId: UserAccountPage }),
  '/settlements/:filter?': () => ({ pageId: SettlementsPage }),
  '/settlement/:msgId?': () => ({ pageId: SettlementPage }),
  '/debug': () => ({ pageId: 'p-debug' })
  // '/debitor/:filter?': () => ({ pageId: DebitorAccount }),
  // '/entitlement/:filter?': () => ({ pageId: EntitlementAccount }),
  // '/income/:filter?': () => ({ pageId: IncomeAccount }),
  // '/queried': () => ({ pageId: QueriedAccount }),
}

const urlMapper = Mapper({ query: true })

// routeLookup :: Object<[pageId]: [route]>
const routeLookup =
  Object
    .keys(routeMappings)
    .reduce((acc, key) => {
      acc[routeMappings[key]().pageId] = key
      return acc
    }, {})

export function parseUrl (url = window.location.hash || (prefix + defaultRoute)) {
  const mapped =
    urlMapper.map(url.substring(prefix.length), routeMappings)

  if (mapped != null) {
    const patch = mapped.match // () => { pageId }
    const params = mapped.values
    return Object.assign({}, patch(), { url, params })
  } else {
    return { pageId: '', url, params: {} }
  }
}

export function getUrl (id, params = {}) {
  const route = routeLookup[id] || defaultRoute
  const result = urlMapper.stringify(route, params)
  return prefix + result // #[route]
}

export function navigateTo (id, params) {
  return parseUrl(getUrl(id, params))
}

export function listenToRouteChanges (update) {
  function emitRouteUpdate () {
    const { pageId, url, params } = parseUrl()
    update(state =>
      Object.assign({}, state, { pageId, url, params }))
    window.scrollTo(0, 0)
  }

  window.onpopstate = emitRouteUpdate
  // emitRouteUpdate() // NOTE: inital state is set in app.initialState
  console.info('[listenToRouteChanges :: router]',
    'router is initialized and listens for \'window.onpopstate\' events...')
}

export function syncLocationBar (state) {
  const getLoc = () => document.location.hash
  const setLoc = path => window.history.pushState({}, '', path)

  if (state.pageId && state.params) {
    const url = getUrl(state.pageId, state.params)

    if (getLoc() !== url) setLoc(url)
  }
  return null
}
