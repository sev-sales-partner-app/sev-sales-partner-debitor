import React from 'react'
import ReactDOM from 'react-dom'
// import { sv } from 'seview'
import b from 'bss/bss.js'
import * as helperthing from 'stylething/bssHelpers'
import * as theme from 'stylething/theme'

const { createElement, Component, Fragment, memo } = React

// seview-react set up

// const h = sv(node => {
//   if (typeof node === 'string') {
//     return node
//   }
//   const attrs = node.attrs || {}
//   if (attrs.innerHTML) {
//     attrs.dangerouslySetInnerHTML = { __html: attrs.innerHTML }
//     delete attrs.innerHTML
//   }
//   const args = [node.tag, node.attrs || {}].concat(node.children || [])
//   return createElement.apply(null, args)
// })
//
// const render = element => view => ReactDOM.render(h(view), element)

const { render } = ReactDOM

// bss helper initialisation
const { helper } = b
const preset = helperthing.createBssHelpers(b)
helper(preset)

export { b, Component, createElement, Fragment, memo, render, preset, theme }
