// _assign :: ({ k: v }, { k: v }) -> { k: v }
const _assign = (a, b) => {
  for (let key in b) if (b.hasOwnProperty(key)) a[key] = b[key]

  return a
}

// // _isArray = x => Boolean
// const _isArray = Array.isArray || function _isArray (x) {
//   return (
//     x != null &&
//     x.length >= 0 &&
//     Object.prototype.toString.call(x) === '[object Array]'
//   )
// }

// _isObject :: x => Boolean
const _isObject = x =>
  Object.prototype.toString.call(x) === '[object Object]'

// // _has :: ({ k: v }) -> k -> Boolean
// const _has = (prop, obj) => {
//   return Object.prototype.hasOwnProperty.call(obj, prop)
// }

// assoc :: k -> v -> { k: v } -> { k: v }
export const assoc = prop => val => obj => {
  const res = _assign({}, obj)
  res[prop] = val
  return res
}

// assocPath :: [k] -> v -> { k: v } -> { k: v }
export const assocPath = ([ head, ...tail ]) => x => obj =>
  // assoc(head, length(tail) ? assocPath(tail, x, obj[head]) : x, obj)
  assoc(head)(
    length(tail)
      ? assocPath(tail)(x)(obj[head])
      : x
  )(obj)

// defaultTo :: a -> a -> a
export const defaultTo = a => b =>
  (b == null) ? a : b

// dissoc :: k -> { k: v } -> { k: v }
export const dissoc = key => obj => {
  const res = _assign({}, obj)
  delete res[key]
  return res
}

// dissocPath :: [k] -> { k: v } -> { k: v }
export const dissocPath = ([ head, ...tail ]) => obj =>
  !head
    ? obj
    : obj[head] == null
      ? obj
      : length(tail)
        ? assoc(head)(dissocPath(tail)(obj[head]))(obj)
        : dissoc(head)(obj)

// length :: [a] -> Number
const length = list => list.length

export const mergeDeepLeft = l => r => {
  let res = _assign({}, _isObject(r) && r)

  if (!_isObject(l)) return res

  for (const k in l) {
    if (!l.hasOwnProperty(k)) continue
    res[k] = _isObject(res[k])
      ? mergeDeepLeft(l[k])(res[k])
      : l[k]
  }

  return res
}

// mergeDeepRight :: ({ k: v}) -> ({ k: v}) -> { k: v }
export const mergeDeepRight = l => r =>
  mergeDeepLeft(r)(l)

// mergeLeft :: { k: v } -> { k: v } -> { k: v }
export const mergeLeft = l => r => mergeRight(r)(l)

// mergeRight :: { k: v } -> { k: v } -> { k: v }
const mergeRight = l => r =>
  reduce(_assign)({})([ l, r ])

// pipe :: ((a -> b), ..., (y -> z)) -> a -> z
export const pipe = (...fns) => (input) =>
  fns.reduce((acc, fn) => fn(acc), input)

// reduce :: Foldable f => (b -> a -> b) -> b -> f a -> b
export const reduce = f => acc => list => list.reduce(f, acc)

// tap :: (a -> b) -> a -> a
export const tap = f => x => {
  f(x)
  return x
}

export const preventDefault = tap(e => e.preventDefault)

// https://codeburst.io/alternative-to-javascripts-switch-statement-with-a-functional-twist-3f572787ba1c
const matched = x => ({
  on: () => matched(x),
  otherwise: () => x
})
export const matchCase = x => ({
  on: (pred, fn) => (pred(x) ? matched(fn(x)) : matchCase(x)),
  otherwise: fn => fn(x)
})
